import itertools

from bokeh.models import HoverTool, DatetimeTickFormatter

from pyreach.reach_config import REACHConfig
from pyreach.repository.models import Temperature, MonitoringProbe, TPMStatus

from flask import Flask, render_template

import numpy as np

import datetime

from bokeh.palettes import Category20 as palette
from bokeh.embed import components
from bokeh.plotting import figure
from bokeh.resources import INLINE


app = Flask(__name__)


def get_temperature_data():
    """ Get temperatures from database and return"""
    # Get temperatures
    temperatures = {}
    for probe in MonitoringProbe.objects():
        values = Temperature.objects(date_time__gte=datetime.datetime.utcnow() - datetime.timedelta(hours=6),
                                     probe=probe)
        temperatures[probe.location] = np.array([[t.date_time for t in values], [t.temperature for t in values]])

    values = TPMStatus.objects(date_time__gte=datetime.datetime.utcnow() - datetime.timedelta(hours=6))
    temperatures['TPM Board'] = np.array([[t.date_time for t in values], [t.board_temperature for t in values]])
    temperatures['TPM FPGA 1'] = np.array([[t.date_time for t in values], [t.fpga1_temperature for t in values]])
    temperatures['TPM FPGA 2'] = np.array([[t.date_time for t in values], [t.fpga2_temperature for t in values]])

    return temperatures


@app.route('/')
def generate_plot():

    temperatures = get_temperature_data()

    p = figure(title="REACH temperatures", x_axis_label='Datetime', y_axis_label='temperature',
               plot_width=1600, plot_height=500, x_axis_type='datetime')

    counter = 0
    for k, v in temperatures.items():
        if v.shape[1] == 0:
            continue

        if '{' in k:
            k = k[:k.index(':') - 1].replace('{', '').replace('[', '').replace("'", '')

        p.line(v[0], v[1], legend_label=k, line_width=2, color=palette[20][counter], alpha=1, muted_alpha=0.5,
               muted=True)
        counter += 1

    p.legend.location = "top_left"
    p.legend.click_policy = "mute"

    p.xaxis.formatter = DatetimeTickFormatter(days="%d/%m %H:%M",
                                              months="%d/%m %H:%M",
                                              hours="%d/%m %H:%M",
                                              minutes="%d/%m %H:%M")
    p.xaxis.major_label_orientation = 1.1
    p.add_tools(HoverTool(tooltips=[('Date', '$data_x{%d/%m-%H:%M}'),
                                    ('Temp', '$data_y{0,0.000000}'), ],
                          formatters={
                              '$data_x': 'datetime',
                          }
                          ))

    # grab the static resources
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()

    # render template
    script, div = components(p)
    html = render_template(
        'index.html',
        plot_script=script,
        plot_div=div,
        js_resources=js_resources,
        css_resources=css_resources,
    )
    return html


if __name__ == '__main__':
    REACHConfig()
    app.run(debug=True)
