echo -e "\n==== Installing REACH scheduler service ====\n"

# Update configuration files
sed -i 's/$REACH__CONFIG_DIRECTORY/'"${REACH__CONFIG_DIRECTORY}"'/' reach_monitor.service
sed -i 's/$REACH__MONGO_USER/'"${REACH__MONGO_USER}"'/' reach_monitor.service
sed -i 's/$REACH__MONGO_PASSWORD/'"${REACH__MONGO_PASSWORD}"'/' reach_monitor.service

# Create and launch reach scheduler service
cp reach_scheduler.service $HOME/.config/systemd/user/
systemctl --user enable reach_scheduler
sudo loginctl enable-linger $USER
systemctl --user daemon-reload
systemctl --user start reach_scheduler