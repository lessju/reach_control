#!/usr/bin/env bash

# Helper function to install required package
function install_package(){
    PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $1 | grep "install ok installed")
    if [[ "" == "$PKG_OK" ]]; then
      echo "Installing $1."
      sudo apt-get -qq --yes install $1 > /dev/null
      return  0 # Return success status
    else
      echo "$1 already installed"
      return 1  # Return fail status (already installed)
    fi
}

# Setup up MONGO database
# Check if mongo PPA is available, if not add it
if ! grep -q "^deb .*mongodb-org*" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv E52529D4
    sudo bash -c 'echo "deb [arch=amd64] http://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" > /etc/apt/sources.list.d/mongodb-org-4.0.list'
    sudo apt -qq update
fi

# Install mongodb package
if install_package mongodb-org; then
    # Set up as a service
    if [[ ! -f /etc/systemd/system/mongodb.service ]]; then
        sudo cp mongodb.service /etc/systemd/system/
    fi

    # Start up service
    sudo service mongod start

    # Check if mongo user password is defined in environment
    if [[ -z "$REACH__MONGO_PASSWORD" ]]; then
        RESULT=0
        while [[ "$RESULT" -eq 0 ]]; do
            echo -n "REACH password is not defined. Please enter a password: "
            read -s REACH__MONGO_PASSWORD
            echo
            echo -n "Re-enter password: "
            read -s PASSWORD2
            echo

            if [[ "$REACH__MONGO_PASSWORD" == "$PASSWORD2" ]]; then
                echo "Password set"
                RESULT=1
            else
                echo "Passwords do not match. Please re-try."
            fi
        done

        # Save password in .bashrc
        echo "export REACH__MONGO_PASSWORD=`echo $REACH__MONGO_PASSWORD`" >> ~/.bashrc
    fi

    # Create REACH root user
    mongo 127.0.0.1:27017/admin --eval "db.createUser({user: 'reach_root', pwd: '$REACH__MONGO_PASSWORD', roles: ['root']})"

    # Create reach normal users
    mongo --port 27017 -u "reach_root" -p "$REACH__MONGO_PASSWORD" --authenticationDatabase "admin" \
          --eval "var password='$REACH__MONGO_PASSWORD'" database_setup.js

    # Override mongo configuration file
    sudo cp mongod.conf /etc

    # Restart mongo service
    sudo service mongod restart

    # Done, source .bashrc
    source ~/.bashrc
fi