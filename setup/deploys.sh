
# Create /opt/reach

# Download fontconfig https://packages.ubuntu.com/bionic/all/fontconfig-config/download
# Instal deb file
# Copy fontconfig files: cp -L -r /etc/fonts/conf.d /tmp/etc-fonts-conf.d
# sudo apt --fix-broken install
# Copy back fontconfig files
# Edit /etc/fonts/fonts.conf and remove the first few lines that start with the tag <its: and the tag <description>
# export TERM=xterm (in ~/.bashrc)

# Download VNA software: https://coppermountaintech.com/download-free-vna-software-and-documentation/ (TRVNA)
# Place in /opt/reach/bin
# Note: Might need to reinstall fontconfig
# Note: Might need to set TERM='xterm'
# Open and set System -> Misc Setup -> Network Setup -> TCP Socket on


# Install libusbtc08-1, libudev-dev

# Create /etc/udev/rules.d/85-minicircuits.rules with:
# ATTR{idVendor}=="20ce", MODE="660", GROUP="plugdev"

# sudo usermod -a -G plugdev $USER
# sudo udevadm control --reload-rules
# sudo udevadm trigger

# Install pyfabil
# Install python package
