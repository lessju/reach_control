db = connect("127.0.0.1:27017/reach");
db.createUser({ user: "reach_ro", pwd: password, roles: [{ role: "read", db: "reach" }] });
db.createUser({ user: "reach_admin", pwd: password, roles: [{ role: "dbAdmin", db: "reach" }] });
db.createUser({ user: "reach_owner", pwd: password, roles: [{ role: "dbOwner", db: "reach" }] });
db.createUser({ user: "reach_rw", pwd: password, roles: [{ role: "readWrite", db: "reach" }] });