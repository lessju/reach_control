# Digital control for REACH

REACH website: https://www.astro.phy.cam.ac.uk/research/research-projects/reach/overview

### Repository Structure

- `config`: Contains configuration files and TPM bitstreams
- `python`: Contains pyreach package and helper scripts
- `setup`: Contains a deployment script to install the REACH backend software (still in development)
- `src`: Contains the ucontroller sketch project

### Basic Installation

**NOTE**: This section will be deprecated once the deployment script is ready

To install the system the following operations are required:
- Create the `/opt/reach` directory, and `log` and `bin` directories within it. Change ownership to `$USER`
- Create a Python 3.9+ virtual environment `/opt/reach/python`
- Install the pyreach package into the virtual environment
- Download and install the TRVNA software and place in `/opt/reach/bin`
- Define the `REACH_CONFIG_DIRECTORY` environmental variable which should point to the `config` directory in the repository

### Running an observation

An observation is defined by a YAML configuration file. The file should be placed in the `config` directory of the repository, 
which contains some pre-defined configurations:
- `instrument.yaml`: Contains the list of controllable devices and their parameters. Should be changed when deploying on a new system
- `switches.yaml`: Contains the pin-configuration of the devices attached to the ucontroller (primarily power and input sources)
- `calibration.yaml`: Defines the list of operations required when calibrating the REACH system
- `test_spectrometer.yaml`: A test configuration for the spectrometer
- `reach.yaml`: A test configuration all the possible operations which can be performed

In the above list, the first two files are instrument configuration files, whilst the 
rest are observation configuration files. The latter define the list of operations
required for a specific observation, where an observation is defined as a list of
operations. Observation configuration files are split into three sections:

`observation`: High-level observation parameters
  - `name`: A name for the observation (used for logging and file generation)
  - `scheduling_mode`: Scheduling format being used (UTC or LST)
  - `start_time`: Date format should be: `%d/%m/%Y_%H:%M`. The word `now` can be used to start immediately
  - `output_directory`:  Base directory where output files will be stored. Ideally should be `/opt/reach/data`. 
  The resulting file path will be: `/base_dir/YYYY_MM_DD/HH_SS/name.hdf5`
  - `spectrometer_channel_id`: ID of the spectrometer channel input to use
  - `longitude`: Longitude of location
  - `latitude`: Latitude of location

`configuration`: Configuration parameters which are common to operations which are often repeated#
  - `vna_wait_time`: Number of seconds to wait for VNA to measure S11
  - `spectrometer_wait_time`: Number of seconds to wait for spectrometer to start acquiring integrations (generally set to 1 and is used to discard the first incomplete spectrum)
  - `integration_time`: Number of seconds to integrate spectra for
  - `number_of_spectra`: The number of integrated spectra to acquire during the `measure_spectrum` operation
  - `dicke_integration_time`: Number of seconds to integrate the cold and noise sources for
  - `dicke_number_of_spectra`: The number of integrated spectra to acquire for the cold and noise sources

`operations`: The ordered list of operation to perform. The primary operations which can be performed include:
- `power_on_spectrometer[: {initialise: True|False}]`: Instantiates spectrometer. The `initialise` parameter can be provided to force initialisation of the spectrometer.
- `power_on_vna`: Launches the VNA GUI
- `power_on_ucontroller*`: Instantiates the ucontroller and enables all power
- `switch_on_mts[: ms2_passthrough]`: Switches on the MTS
- `switch_off_mts`: Switches off the MTS
- `calibrate_vna: {power_level: value, test_source: value[, accuracy: value, max_retries: value, measured_pnax: value]}`: Calibrate the VNA using internal sources, and test against
specified test source. If a `measured_pnax` is provided, the test source computed value is compared against it, and if it within `accuracy` then the calibration will be regarded as successful. If not the procedure will be retried up to `max_retries` times.
- `measure_s: {name: desc_name, source: source_name}`: Measures S11 parameter for a particular source.  The source must be defined in switches.yml.
- `generate_calibration_structures[: {equalize: False, decimate: False}]`: Extract the generated H5 file contents into a directory structure for running through the calibration procedure.
- `calculate_calibration_coefficients`: Calculates the calibration coefficients, not implemented
- `measure_spectrum: {name: desc_name, source: source_name[, three_way_switching: True]}`: Measures a spectrum for a particular source.  The source must be defined in switches.yml.
The cold and load sources will also be measured if `three_way_switching` is enabled (default).
- `[disable|enable]_power_supply: {supply: supply_name}`: Enable or disable a power supply. `supply_name` must be listed in `switches.yaml`
- `sleep: {duation: seconds}`: Sleep for specified duration
- `power_off_[spectrometer|vna|ucontroller]`: Powers off the specified device

If certain operation need to be performed repeatedly, they can be encased as `observation_operations`, which
contains three additional parameters:
- `start_time`: Time to start the repeated operations
- `repetitions`: Number of times to repeat the operations
- `every`: Number of seconds between repetitions

Additional Notes:
- VNA files will be copied to the output directory
- All measured spectra will be saved in a single HDF5, which will contains two datasets
per source, once containing the spectra and the other containing the corresponding timestamps (Unix time and LST).

To run an observation, use the `observation.py` script:
`cd repo_path\python\pyreach`
`python observation.py --config=config_file_name.yml [--dry-run]`
The configuration file should be relative to the directory in `REACH_CONFIG_DIRECTORY`. If the dry run flag is included, then the script will list the operations which it will perform, but not do them.

### Monitoring and Control

The temperature and power probes are handled by a service which runs continuously in the background. The service will start on system startup, and will periodically
read all the sensors and store the information in a Mongo database. The following operations can be performed on the service:
- `systemctl --user start reach_monitor`: Start the service
- `systemctl --user start reach_monitor`: Stop the service
- `systemctl --user status reach_monitor`: Check the status of the service

If a probe is detached and re-attached whilst the service is running, it is suggested that the service be restarted.

Power control is provided through a Python script, `tripp_lite_pdu.py`. Running the script will print out the status of
all the PDU ports. Individual ports can also be switched on or off using the `--action` parameter that accepts `status`, 
`switch_on`, `switch_off` and `reset`. The ports on which this action will be performed can be specified either
by providing the port numbers to `--ports` or by specifying the device name itself in `device`. The device name must
match the entries in `instrument.yaml`, section  `pdu`.

The TEC TCM temperature probe switched on by using the `tec_tcm.py` script with the `--configure` argument. This needs to be performed
when `reach_monitor` is not running.