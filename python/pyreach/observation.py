#!/usr/bin/env python

import logging
import os
import signal
import subprocess
import threading
import time
from datetime import datetime, timedelta

import h5py
import numpy as np
import pytz

from pyreach import utils
from pyreach.core.slack import ReachSlack, DummySlack
from pyreach.digital_backend.spectrometer import Spectrometer
from pyreach.instrument.devices import Device
from pyreach.instrument.ucontroller.microcontroller import Microcontroller
from pyreach.instrument.vna.vna import VNA
from pyreach.reach_config import REACHConfig
from pyreach.repository.metric_data import MetricData
from pyreach.repository.models import MonitoringProbe
from pyreach.service.calibration import CalibrationWrapper
from pyreach.utils import check_vna_calibration


class REACHObservation:
    """ Class which implements the functionality for the observation """

    def __init__(self, observation, operations, configuration, override_signal_handler=True):
        """ Class constructor
        :param observation: Observation parameters in dictionary format
        :params operations: List of operations """

        # Sanity check
        if not type(observation) is dict:
            logging.error("No observation parameters defined")
        if not type(operations) is list and len(operations) == 0:
            logging.error("No operations defined")

        # Store observation parameters and operations
        self._simulation_mode = False
        self._simulate_ucontroller = False
        self._observation_name = observation.get("name", "test_observation")
        self._start_time = observation.get("start_time", "NOW").upper()
        self._scheduling_mode = observation.get("scheduling_mode", "UTC").upper()
        self._output_directory = observation.get("output_directory", "/tmp/reach_test_obs")
        self._longitude, self._latitude = observation.get("longitude"), observation.get("latitude")
        self._spectrometer_channel_id = observation.get("spectrometer_channel_id", 0)
        self._operations = operations

        # During a dry run, the estimated observation runtime is calculated
        self._estimated_observation_runtime = 0

        # If start time is "now", set to current time
        if self._start_time == "NOW":
            self._start_time = datetime.utcnow() + timedelta(seconds=5)
            self._start_time.replace(microsecond=0)
            self._start_time.replace(tzinfo=pytz.utc)
        else:
            self._start_time = datetime.strptime(self._start_time, "%d/%m/%Y_%H:%M")
            self._start_time.replace(tzinfo=pytz.utc)

        # Instantiate slack
        if 'notify_on_slack' in REACHConfig()['observation'].keys() and REACHConfig()['observation']['notify_on_slack']:
            self._slack = ReachSlack(REACHConfig()['software_framework']['slack_token_file'])
        else:
            self._slack = DummySlack()

        # Sanity check on start time
        if self._start_time < datetime.utcnow():
            logging.error("Start time must be 'now' or at least 5 seconds in the future")
            exit()

        # Generate output directory for current observation
        self._output_directory = os.path.join(*[self._output_directory,
                                                self._start_time.strftime("%Y_%m_%d"),
                                                self._start_time.strftime("%H_%M")])

        # Check if directory exists, and if not try to create it
        if not os.path.exists(self._output_directory):
            try:
                os.makedirs(self._output_directory)
            except IOError:
                logging.error(f"Could not create output directory {self._output_directory}")
                return
        elif os.path.isfile(self._output_directory):
            logging.error("Specified output path {self._output_directory} is a file, must be a directory")

        # Placeholder for hardware devices
        self._spectrometer = None
        self._ucontroller = None
        self._vna = None

        # Operation tracking
        self._temperatures_saved = False

        # Placeholder for spectra data file
        self._observation_data_file = os.path.join(self._output_directory,
                                                   f"{self._observation_name}.hdf5")

        # Load general operation configuration parameters
        self._vna_wait_time = 0
        self._integration_time = 1
        self._number_of_spectra = 1
        self._spectrometer_wait_time = 1
        if configuration is not None:
            self._vna_wait_time = configuration.get('vna_wait_time', self._vna_wait_time)
            self._integration_time = configuration.get('integration_time', self._integration_time)
            self._number_of_spectra = configuration.get('number_of_spectra', self._number_of_spectra)
            self._spectrometer_wait_time = configuration.get('spectrometer_wait_time', 1)
            self._dicke_integration_time = configuration.get('dicke_integration_time', self._integration_time)
            self._dicke_number_of_spectra = configuration.get('dicke_number_of_spectra', self._number_of_spectra)

        # Assigned signal handler
        self._override_signal_handler = override_signal_handler
        if override_signal_handler:
            self._original_signal_handler = self.register_signal_handler()

    def run_observation(self, operations=None, top_level=True):
        """ Run observation, go through all operations
        :param operations: List of operations to run. If this is not specified
                           then the locally saved operations will be used
        :param top_level: Specify that this call is processing the top-level operations """

        # Wait for observation to start
        if not self._simulation_mode:

            # Check that background service is running
            if 'reach_monitor_required' in REACHConfig()['software_framework'].keys() and \
                    REACHConfig()['software_framework']['reach_monitor_required']:
                if not self._check_reach_monitor():
                    return

            self._slack.info(f"Observation {self._observation_name} will start at {self._start_time}")
            utils.schedule(self._start_time, mode=self._scheduling_mode)
        else:
            if top_level:
                logging.info(f"Operations will start at {self._start_time}")

        # If a list of operations is not passed on as an argument then use
        # the list in the object instance
        if operations is None:
            operations = self._operations

        # Create output file
        if not self._simulation_mode:
            self._create_output_file()

        # Go through list of operations and run them
        for operation in operations:
            if type(operation) is not dict:

                try:
                    self.run_operation(operation)
                except Exception as e:
                    logging.error(f"Error while performing operation {operation}: {e}")
                    self._slack.info(f"Error while performing operation {operation}")
                    self._kill()
            else:
                key = list(operation.keys())
                if len(key) != 1:
                    logging.error(f"Operation entry can only have one key ({operation} is invalid). Skipping")
                    continue

                try:
                    self.run_operation(key[0], operation[key[0]])
                except Exception as e:
                    logging.error(f"Error while performing operation {operation}: {e}")
                    self._slack.info(f"Error while performing operation {operation}")
                    self._kill()

        # Finished performing all operations, add metric metadata to file if required
        if top_level:
            try:
                self._add_receiver_temperatures_to_file()
            except Exception as e:
                logging.error(f"Error while adding receiver temperatures to file: {e}")
                self._slack.error(f"Error while adding receiver temperatures to file")

            try:
                self._add_tpm_temperatures_to_file()
            except Exception as e:
                logging.error(f"Error while adding TPM temperatures to file: {e}")
                self._slack.error(f"Error while adding TPM temperatures to file")

            self._slack.info(f"Observation {self._observation_name} finished")

    def run_operation(self, operation, parameters=None):
        """ Execute an operation
        :param operation: Operation to be performed
        :param parameters: Dict containing any required parameters """

        # Execute the function associated with the operation
        if operation == "power_on_spectrometer":
            self._initialise_spectrometer(
                initialise=parameters.get('initialise', False) if parameters is not None else False)
        elif operation == "power_on_vna":
            self._initialise_vna()
        elif operation == "power_on_ucontroller":
            self._initialise_ucontroller()
        elif operation == "simulate_ucontroller":
            self._initialise_ucontroller(simulate=True)
        elif operation == "power_off_ucontroller":
            self._switch_off_ucontroller()
        elif operation == "power_off_vna":
            self._switch_off_vna()
        elif operation == "power_off_spectrometer":
            self._switch_off_spectrometer()
        elif operation == "switch_on_mts":
            self._switch_on_mts(parameters.get("ms2_passthrough", False) if parameters is not None else False)
        elif operation == "switch_off_mts":
            self._switch_off_mts()
        elif operation == "keep_switch_on":
            self.update_always_on_switches(parameters['switch'], parameters['enabled'], parameters.get("port", 1))
        elif operation == "enable_port":
            self._enable_port(parameters.get("switch"), parameters.get("port"))
        elif operation == "enable_power_supply":
            self._enable_power_supply(supply=parameters['supply'])
        elif operation == "disable_power_supply":
            self._disable_power_supply(supply=parameters['supply'])
        elif operation == "calibrate_vna":
            self._calibrate_vna(power_level=parameters.get('power_level', None),
                                accuracy=parameters.get('accuracy', 1),
                                test_source=parameters.get('test_source', None),
                                measured_pnax=parameters.get('measured_pnax', None),
                                max_retries=parameters.get("max_retries", 3))
        elif operation == "measure_s":
            self._measure_s(parameters['name'], parameters['source'])
        elif operation == "switch_to_source":
            self._switch_to_source(parameters['source'])
        elif operation == "measure_spectrum":
            self._measure_spectrum(parameters['name'],
                                   parameters['source'],
                                   parameters.get('three_way_switching', True))
        elif operation == "calculate_calibration_coefficients":
            self._calculate_calibration_coefficients()
        elif operation == "generate_calibration_structures":
            if parameters is None:
                self._generate_calibration_structures()
            else:
                self._generate_calibration_structures(equalize=parameters.get('equalize', False),
                                                      decimate=parameters.get('decimate', False),
                                                      match_psd_lengths=parameters.get('match_psd_lengths', False))
        elif operation == "sleep":
            if self._simulation_mode:
                self._estimated_observation_runtime += parameters['duration']
            else:
                logging.info(f"Sleeping for {parameters['duration']}s")
                time.sleep(parameters['duration'])

        # Main observation with repeated readings
        elif operation == "observation_operations":
            # Running a repeated obs. Check start time and repetition period
            local_start = parameters.get('start_time', "now")
            period = parameters.get("every", 0)

            # If "now", then schedule in 5 seconds
            if local_start == "now":
                local_start = datetime.utcnow() + timedelta(seconds=5)
                local_start.replace(microsecond=0)
            else:
                # If not "now", then parse the time string
                local_start = datetime.strptime(local_start, "%d/%m/%Y_%H:%M")
                local_start = pytz.utc.localize(local_start)

            # Wait for observation start
            if not self._simulation_mode:
                utils.schedule(local_start, mode=self._scheduling_mode)
            else:
                logging.info(f"Observation will start at {self._start_time}")

            # Process the repeating observation operations
            for i in range(parameters.get('repetitions', 1)):
                # Update start time to reflect current iteration
                if period == "now":
                    self._start_time = "now"
                else:
                    self._start_time = local_start + timedelta(seconds=period * i)

                self.run_observation(parameters.get('operations', []), top_level=False)
        else:
            logging.warning(f"Operation {operation} not supported. Skipping")

    def dry_run_operations(self):
        """ Perform a dry run of the operations to make sure that configuration is valid """

        # Enable simulation mode
        self._simulation_mode = True
        self._estimated_observation_runtime = 0
        prev_slack = self._slack
        self._slack = DummySlack()

        # Run operations
        self.run_observation()

        # Disable simulation mode
        self._simulation_mode = False
        self._slack = prev_slack

        # Reset signal handler
        if self._override_signal_handler:
            self.register_signal_handler(self._original_signal_handler)

        return self._estimated_observation_runtime

    def _create_output_file(self):
        """ Create HDF5 output file """

        # If file already exists, do not overwrite
        if os.path.exists(self._observation_data_file):
            return

        # Create HDF5 file which will store observation data
        with h5py.File(self._observation_data_file, 'w') as f:
            # Create group which will contain observation info
            info = f.create_group('observation_info')

            # Add attributes to group
            info.attrs['observation_name'] = self._observation_name
            info.attrs['start_time'] = self._start_time.timestamp()
            info.attrs['start_lst'] = utils.get_sidereal_time(self._longitude, self._latitude,
                                                              self._start_time.timestamp())

            # TODO: Add reference to config file, or config file contents, in the HDF5 metadata
            # info.attrs['operations'] = self._operations

            # TODO: Add more when required
            # ...

            # Create group which will contain all observation data
            f.create_group("observation_data")

        logging.info(f"Created output file {self._observation_data_file}")

    def _add_spectrum_to_file(self, spectrum, source, name, timestamp):
        """ Add spectrum to data file
        :param spectrum: The spectrum
        :param source: The source being observed
        :param name: The data name
        :param timestamp: Spectrum timestamp """

        nof_frequency_channels = REACHConfig()['spectrometer']['nof_frequency_channels']

        with h5py.File(self._observation_data_file, 'a') as f:
            # Create dataset names
            dataset_name = f"{name}_spectra"
            timestamp_name = f"{name}_timestamps"

            # Load observation data group
            dset = f['observation_data']

            # If data sets do not exist, add them
            if dataset_name not in dset.keys():
                dset.create_dataset(dataset_name,
                                    (0, nof_frequency_channels),
                                    maxshape=(None, nof_frequency_channels),
                                    chunks=True,
                                    dtype='f8')
                f[f'observation_data/{dataset_name}'].attrs['source_id'] = source

                dset.create_dataset(timestamp_name, (0, 2),
                                    maxshape=(None, 2), chunks=True, dtype='f8')

                logging.info(f"Added {dataset_name} dataset to output file")

            # Add spectrum to buffer
            dset = f[f'observation_data/{dataset_name}']
            dset.resize((dset.shape[0] + 1, dset.shape[1]))
            dset[-1, :] = spectrum

            # Calculate sidereal time and set timestamp to UTC timezone
            sidereal = utils.get_sidereal_time(self._longitude, self._latitude, timestamp)
            timestamp = datetime.utcfromtimestamp(timestamp).timestamp()

            # Add timestamp and lst time to buffer
            dset = f[f'observation_data/{timestamp_name}']
            dset.resize((dset.shape[0] + 1, dset.shape[1]))
            dset[-1, :] = [timestamp, sidereal]

    def _add_receiver_temperatures_to_file(self):
        """ Function which should be called at the end of an observation, or run, to load measured device temperatures
          from the metric database and add them to the HDF5 file"""

        # Check whether we should store temperatures to file
        if not REACHConfig()['observation'].get('store_receiver_temperatures', False) or self._simulation_mode \
                or self._temperatures_saved:
            return

        # If so, determine how many channels are needed
        nof_channels = len(REACHConfig()['tc08_temperature_sensor']['channel_names'])

        # Get probe locations
        probe_locations = [p.location for p in MonitoringProbe.objects(device=Device.TC08).order_by("sensor_index")]

        # Get values from database
        metric_cadence = REACHConfig()['software_framework']['instrument_monitor_cadence']
        temperatures = MetricData().get_receiver_temperatures(
            start_date=self._start_time - timedelta(seconds=metric_cadence),
            devices=[(Device.TC08, i) for i in range(nof_channels)])

        # Temperatures are grouped by device and sensor location, each sorted in ascending order in time.
        # To be strict, check that each "row" of temperatures corresponds to the same timestamp.

        # Use the same number of rows for each sensor
        nof_rows = min([len(t) for t in temperatures.values()])

        # If no temperatures were found, return
        if nof_rows == 0:
            logging.warning("No receiver temperature data was found for observation time frame!")
            return

        # Create temperature (and timestamp) store and loop through the temperature values
        temperature_store = np.zeros((nof_rows, nof_channels), dtype=np.float32)
        timestamp_store = np.zeros((nof_rows,), dtype=float)
        max_delta = timedelta(seconds=1)
        for i in range(nof_rows):
            # Grab the timestamps for the current row and check that they are close. If they are
            # within a second apart, add to the array, otherwise skip
            times = np.array([v[i][0] for k, v in temperatures.items()])
            if np.all(np.abs(times - times[0])) < max_delta:
                temperature_store[i, :] = [v[i][1] for k, v in temperatures.items()]
                timestamp_store[i] = times[0].timestamp()
            else:
                logging.warning("Discrepancy in temperature timings, skipping row")

        # Convert temperatures to Kelvin
        temperature_store += 273.15

        # Add data to HDF5
        with h5py.File(self._observation_data_file, 'a') as f:

            # Create and Load observation metadata group
            if "observation_metadata" not in f:
                dset = f.create_group('observation_metadata')
            else:
                dset = f['observation_metadata']

            # Add probe information
            dset.attrs['receiver_temperature_probe_locations'] = probe_locations

            # Create dataset
            dset.create_dataset("temperatures",
                                (nof_rows, nof_channels),
                                maxshape=(nof_rows, nof_channels),
                                dtype='f4')

            dset.create_dataset("temperature_timestamps",
                                (nof_rows,),
                                maxshape=(nof_rows,),
                                dtype='f8')

            # Add temperatures and timestamps to file
            dset = f[f'observation_metadata/temperatures']
            dset[:] = temperature_store

            dset = f[f'observation_metadata/temperature_timestamps']
            dset[:] = timestamp_store

        logging.info("Receiver probe temperatures saved to file")
        self._temperatures_saved = True

    def _add_tpm_temperatures_to_file(self):
        """ Function which should be called at the end of an observation, or run, to load measured tpm temperatures
          from the metric database and add them to the HDF5 file"""

        # Check whether we should store temperatures to file
        if not REACHConfig()['observation'].get('store_tpm_temperatures', False) or self._simulation_mode:
            return

        # Get values from database
        temperatures = MetricData().get_tpm_temperatures(start_date=self._start_time - timedelta(seconds=60))

        # If no temperatures were found, return
        if len(temperatures) == 0:
            logging.warning("No TPM temperature data was found for observation time frame!")
            return

        # Add data to HDF5
        with h5py.File(self._observation_data_file, 'a') as f:

            # Create and Load observation metadata group
            if "observation_metadata" not in f:
                dset = f.create_group('observation_metadata')
            else:
                dset = f['observation_metadata']

            # Add probe information
            dset.attrs['tpm_temperature_probe_locations'] = ["board", "fpga1", "fpga2"]

            # Create dataset
            dset.create_dataset("tpm_temperatures",
                                (len(temperatures), 3),
                                maxshape=(len(temperatures), 3),
                                dtype='f4')

            dset.create_dataset("tpm_temperature_timestamps",
                                (len(temperatures),),
                                maxshape=(len(temperatures),),
                                dtype='f8')

            # Add temperatures and timestamps to file
            dset = f[f'observation_metadata/tpm_temperatures']
            dset[:] = [t[1:] for t in temperatures]

            dset = f[f'observation_metadata/tpm_temperature_timestamps']
            dset[:] = [t[0].timestamp() for t in temperatures]

        logging.info("TPM temperatures saved to file")

    def _measure_s(self, name, source):
        """ Measure S parameters of a specific source """

        # Simulation mode logging
        if self._simulation_mode:
            logging.info(f"S parameters for {name} will be measured")
            self._estimated_observation_runtime += REACHConfig()['vna']['average'] + self._vna_wait_time
            return

        self._slack.info(f"Measuring S11 for {source}")

        # Sanity check
        if self._vna is None or (self._ucontroller is None and not self._simulate_ucontroller):
            logging.error("VNA and ucontroller must be initialised to measure S parameters")
            exit()

        # Toggle switch
        if source != "none":
            self._switch_to_source(source)

        # Wait for a while
        time.sleep(self._vna_wait_time)

        # Perform sweep
        self._vna.measure_s11()

        # Measure with VNA
        self._vna.snp_save(name)

        # Ready from source
        if source != "none":
            self._switch_source_off(source)

        logging.info(f"Measured S parameters for {name}")

    def _perform_spectrum_measurement(self, name, source, integration_time, number_of_spectra):
        """ Helper function to perform spectrum measurement """
        # Switch to source
        if source != "none":
            self._switch_to_source(source)

        # Get required spectra
        timestamps, spectra = self._spectrometer.acquire_spectrum(channel=self._spectrometer_channel_id,
                                                                  nof_seconds=integration_time * number_of_spectra,
                                                                  wait_seconds=self._spectrometer_wait_time,
                                                                  integrate=False)

        # Combine spectra in groups of integration time and save to file
        for i in range(number_of_spectra):
            start = i * integration_time
            stop = (i + 1) * integration_time
            self._add_spectrum_to_file(np.sum(spectra[start: stop], axis=0), source, name, timestamps[start])

        # Ready from source
        if source != "none":
            self._switch_source_off(source)

        logging.info(f"Measured spectra for {name}")

    def _measure_spectrum(self, name, source, three_way_switching=True):
        """ Measure spectrum of specific source. Note that cold and load source will also be measured """

        # Simulation mode logging
        if self._simulation_mode:
            logging.info(f"Spectrum for {source} will be measured")
            self._estimated_observation_runtime += self._number_of_spectra * \
                                                   (self._integration_time + self._spectrometer_wait_time)
            if three_way_switching:
                self._estimated_observation_runtime += 2 * self._dicke_number_of_spectra * \
                                                       (self._integration_time + self._spectrometer_wait_time)
            return
        else:
            logging.info(f"Measuring spectrum for {source}")
            self._slack.info(f"Measuring spectrum for {source}")

        # Sanity check
        if self._spectrometer is None:
            logging.error("Spectrometer must be initialised to measure spectra.")
            exit()

        if source != "none" and self._ucontroller is None and not self._simulate_ucontroller:
            logging.error("Microcontroller must be initialised to measure spectra.")
            exit()

        # Dicke switching procedure should run through source, load, noise source
        self._perform_spectrum_measurement(name, source, self._integration_time, self._number_of_spectra)

        if three_way_switching:
            self._perform_spectrum_measurement(f"{name}_load", "50_ohm_cold",
                                               self._dicke_integration_time,
                                               self._dicke_number_of_spectra)
            self._perform_spectrum_measurement(f"{name}_ns", "noise_source",
                                               self._dicke_integration_time,
                                               self._dicke_number_of_spectra)

    def _calibrate_vna(self, power_level=None, accuracy=-1, max_retries=3,
                       test_source=None, measured_pnax=None):
        """ Calibrate VNA """

        # Simulation mode logging
        if self._simulation_mode:
            logging.info("VNA will be calibrated")
            self._estimated_observation_runtime += 3 * self._vna_wait_time * REACHConfig()['vna']['average'] + 2
            return

        self._slack.info(f"Calibrating VNA at level {power_level} using source {test_source}")

        # Sanity check
        if self._vna is None:
            logging.error("VNA must be initialised to calibrate VNA.")
            exit()

        # Set VNA power level if specified
        if power_level is not None:
            self._vna.power_level(power_level)

        # Perform calibration
        successful = True
        for i in range(max_retries):

            # Switch to internal source
            self._vna.set_trigger_source(internal=True)

            # Measure short
            self._switch_to_source("vna_short")
            time.sleep(self._vna_wait_time)
            self._vna.calibrate('short')

            # Measure open
            self._switch_to_source("vna_open")
            time.sleep(self._vna_wait_time)
            self._vna.calibrate('open')

            # Measure load
            self._switch_to_source("vna_load")
            time.sleep(self._vna_wait_time)
            self._vna.calibrate('load')
            self._switch_source_off("vna_load")

            # Apply calibration and reset trigger
            self._vna.calibrate("apply")
            time.sleep(1)
            self._vna.set_trigger_source(internal=False)

            logging.info("Performed VNA calibration")

            # Save calibration
            self._vna.state_save("calibration_state")
            logging.info("Saved VNA calibration")

            # If a test source is specified, measure S11 for source
            if test_source is not None:
                self._measure_s(test_source, test_source)

            # Check if calibration was successful
            if measured_pnax is not None:
                try:
                    measured_pnax = complex(measured_pnax)
                    check_vna_calibration(os.path.join(self._output_directory, f'{test_source}.s1p'),
                                          measured_pnax, accuracy)
                    logging.info(f"Receiver {test_source} matches measured PNAX measurement within {accuracy}%")
                    successful = True
                    break
                except ValueError as e:
                    successful = False
                    logging.error(f"Could not check calibration for {test_source} {i + 1} of {max_retries}: {e}")
                except AssertionError:
                    successful = False
                    logging.info(f"Receiver {test_source} does not match PNAX measurement "
                                 f"to within {accuracy}% - {i + 1} of {max_retries}")
            else:
                successful = True

        if not successful:
            logging.error(f"VNA calibration failed")
            self._slack.error(f"Calibrating VNA failed!")
            self._vna.terminate()
            exit()
        else:
            self._slack.info(f"VNA successfully calibrated!")

    def _generate_calibration_structures(self, equalize=False, decimate=False, match_psd_lengths=False):
        """ Generate directory structure for calibration using data in the H5 file and generate by the VNA """

        # Simulation mode logging
        if self._simulation_mode:
            logging.info("Generating directory structure for calibration")
            self._spectrometer_wait_time += 1
            return

        # Place temperatures in file
        self._add_receiver_temperatures_to_file()

        # Create calibration directory
        calibration = CalibrationWrapper(self._output_directory)
        calibration.generate_calibration_directory(decimate=decimate, equalize=equalize,
                                                   match_psd_lengths=match_psd_lengths)

    def _calculate_calibration_coefficients(self):
        """ Determine calibration coefficients """
        # Simulation mode logging
        if self._simulation_mode:
            logging.info("Calibration coefficients will be calculated")
            return

        # TODO: Call Ian's code
        return

    def _switch_to_source(self, source):
        """ Enable source through microcontroller
        :param source: Source defined in switches """

        # Simulation mode logging
        if self._simulation_mode or self._simulate_ucontroller:
            logging.info(f"Source {source} will be enabled")
            return

        # Switch to source
        self._ucontroller.switch_to_source(source)

    def _switch_source_off(self, source):
        """ Disable a source through microcontroller
        :param source: Source defined in switches """
        # Simulation mode logging
        if self._simulation_mode or self._simulate_ucontroller:
            logging.info(f"Source {source} will be disabled")
            return

        # Switch to source
        self._ucontroller.disable_source(source)

    def _switch_on_mts(self, ms2_passthrough=False):
        """ Switch on the MTS switch """

        # Simulation mode logging
        if self._simulation_mode or self._simulate_ucontroller:
            logging.info("MTS will be switched on")
            return

        self._ucontroller.enable_mts(ms2_passthrough)
        logging.info(f"Enabled MTS with MS2 passthrough {ms2_passthrough}")

    def _switch_off_mts(self):
        """ Switch on the MTS switch """

        # Simulation mode logging
        if self._simulation_mode or self._simulate_ucontroller:
            logging.info("MTS will be switched off")
            return

        self._ucontroller.disable_mts()
        logging.info("Disabled MTS")

    def update_always_on_switches(self, switch, enabled, port):
        """ Update list of always on switches """
        # Simulation mode logging
        if self._simulation_mode or self._simulate_ucontroller:
            logging.info(f"Switch {switch} always on for port {port} enable: {enabled}")
            return

        if enabled:
            self._ucontroller.add_always_on_switch(switch, port)
            logging.info(f"Switch {switch} always on enabled on port {port}")
        else:
            self._ucontroller.remove_always_on_switch(switch)
            logging.info(f"Switch {switch} always on disabled")

    def _enable_power_supply(self, supply):
        """ Enable power supply on ucontroller """
        # Simulation mode logging
        if self._simulation_mode:
            logging.info(f"Power supply {supply} will be enabled")
            return

        # Sanity check
        if self._ucontroller is None and not self._simulate_ucontroller:
            logging.error("ucontroller must be initialised to enable power supply")
            exit()

        # Enable power supply
        self._ucontroller.enable_power_source(supply)

    def _enable_port(self, switch, port):
        """ Enable a specific port on a switch """
        # Simulation mode logging
        if self._simulation_mode:
            logging.info(f"Port {port} on switch {switch} will be enabled")
            return

        # Sanity check
        if self._ucontroller is None and not self._simulate_ucontroller:
            logging.error("ucontroller must be initialised to enable a port")
            exit()

        self._ucontroller.enable_port(switch, port)
        logging.info(f"Enabled port {port} on switch {switch}")

    def _disable_power_supply(self, supply):
        """ Enable power supply on ucontroller """
        # Simulation mode logging
        if self._simulation_mode:
            logging.info(f"Power supply {supply} will be enabled")
            return

        # Sanity check
        if self._ucontroller is None and not self._simulate_ucontroller:
            logging.error("ucontroller must be initialised to enable power supply")
            exit()

        # Enable power supply
        self._ucontroller.disable_power_source(supply)

    def _initialise_ucontroller(self, simulate=False):
        """ Initialise ucontroller """

        # Set ucontroller simulation flag
        self._simulate_ucontroller = simulate

        # Simulation mode logging
        if self._simulation_mode or self._simulate_ucontroller:
            logging.info("Microcontroller will be initialized")
            return

        if self._ucontroller is not None:
            logging.warning("uController already initialized, skipping")
            return

        _ = REACHConfig()['ucontroller']
        self._ucontroller = Microcontroller()

        logging.info("Initialised ucontroller")

    def _initialise_vna(self):
        """ Initialise VNA """

        # Simulation mode logging
        if self._simulation_mode:
            logging.info("VNA will be initialised")
            self._estimated_observation_runtime += 60
            return

        if self._vna is not None:
            logging.warning("VNA already initialised, skipping")
            return

        # Reset signal handler
        if self._override_signal_handler:
            self.register_signal_handler(self._original_signal_handler)

        # Create VNA instance
        conf = REACHConfig()['vna']
        self._vna = VNA(conf['gui_path'], save_directory=conf['save_directory'], copy_directory=self._output_directory)
        self._vna.configure(channel=conf['channel'],
                            start_frequency=conf['start_frequency'],
                            stop_frequency=conf['stop_frequency'],
                            ifbw=conf['ifbw'],
                            average=conf['average'],
                            calibration_kit=conf['calibration_kit'],
                            power_level=conf['power_level'],
                            enable_debug=conf['enable_debug'],
                            nof_points=conf['nof_points'])

        # Re-apply signal handler
        if self._override_signal_handler:
            self.register_signal_handler()

        logging.info("Initialised VNA")

    def _initialise_spectrometer(self, initialise=False):
        """ Initialise spectrometer """

        # Simulation mode logging
        if self._simulation_mode:
            logging.info(f"Spectrometer will be initialised (initialise={initialise})")
            self._estimated_observation_runtime += 20
            return

        if self._spectrometer is not None:
            logging.warning("Spectrometer already initialised, skipping")
            return

        # Create spectrometer instance
        conf = REACHConfig()['spectrometer']
        self._spectrometer = Spectrometer(ip=conf['ip'], port=conf['port'],
                                          lmc_ip=conf['lmc_ip'], lmc_port=conf['lmc_port'])
        self._spectrometer.connect()

        if initialise or not self._spectrometer.is_programmed():

            if not self._spectrometer.is_programmed():
                logging.info("Spectrometer is not programmed, initialising")
            else:
                logging.info("Initialising spectrometer")

            bitstream = os.path.join(os.environ['REACH_CONFIG_DIRECTORY'], conf['bitstream'])
            self._spectrometer.program(bitstream)
            self._spectrometer.initialise(channel_truncation=conf['channel_truncation'],
                                          integration_time=conf['integration_time'],
                                          ada_gain=conf['ada_gain'])

        logging.info("Initialised spectrometer")

    def _switch_off_ucontroller(self):
        """ Switch off ucontroller """
        # Simulation mode logging
        if self._simulation_mode:
            logging.info("Ucontroller will be switched off")
            return

        # Power off the ucontroller
        if self._ucontroller is not None:
            self._ucontroller.place_on_standby()

        logging.info("Placed ucontroller on standby")

    def _switch_off_vna(self):
        """ Switch off VNA """
        # Simulation mode logging
        if self._simulation_mode:
            logging.info("VNA will be switched off")
            return

        # Terminate VNA
        if self._vna is not None:
            self._vna.terminate()

        logging.info("Powered off VNA")

    def _switch_off_spectrometer(self):
        """ Switch off spectrometer """
        # Simulation mode logging
        if self._simulation_mode:
            logging.info("Spectrometer will be switched off")
            return

        # TODO: Reset PDU power to TPM
        logging.info("Powered off spectrometer")

    @staticmethod
    def _check_reach_monitor_error():
        """ Check if there are any errors in the reach_monitor """
        stat = subprocess.check_output(["systemctl", "--user", "status", "reach_monitor"])
        return "Could not connect to TC08" in stat.decode()

    def _check_reach_monitor(self):
        """ Check if reach service monitor is active, and if not start it """

        # Check if service is running
        stat = subprocess.call(["systemctl", "--user", "is-active", "--quiet", "reach_monitor"])

        if stat == 0:
            # Service is running, check if there are any errors
            if self._check_reach_monitor_error():
                logging.critical("REACH Monitor has errors. Check service")
                return False
            else:
                logging.info("REACH Monitor is running")

        else:
            # Service is not running, start it
            subprocess.call(["systemctl", "--user", "start", "--quiet", "reach_monitor"])
            logging.info("REACH Monitor is not running, starting it")

            time.sleep(5)

            # Check if service has been started
            stat = subprocess.call(["systemctl", "--user", "is-active", "--quiet", "reach_monitor"])
            if stat == 0:
                # Check if there were any errors
                if self._check_reach_monitor_error():
                    logging.critical("REACH Monitor started but has errors. Check service")
                    return False
                else:
                    logging.info("REACH Monitor started successfully")
            else:
                logging.critical("REACH Monitor could not be started, please check")
                return False

        return True

    def _kill(self):
        """ Called when observation needs to be killed (interrupt or error) """
        if self._vna:
            self._vna.terminate()

        if self._ucontroller:
            self._ucontroller.place_on_standby()

        exit()

    def register_signal_handler(self, external_function=None):
        def signal_handler(signum, frame):
            # Stop observer and data acquisition
            logging.info("Received interrupt, force stopping observation")
            self._kill()

        # If this is not running in the main thread return, signals cannot be assigned in other threads
        if not threading.main_thread():
            return

        if external_function is not None:
            return signal.signal(signal.SIGINT, external_function)
        else:
            return signal.signal(signal.SIGINT, signal_handler)


if __name__ == "__main__":
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("--config", dest="config_file", default="reach",
                      help="Configuration file (default: reach)")
    parser.add_option("--dry-run", dest="dry_run", default=False, action="store_true",
                      help="Perform dry run of operations")
    (options, args) = parser.parse_args()

    # Sanity check on provided config file
    if not options.config_file.endswith(".yaml"):
        options.config_file += ".yaml"

    # Check that REACH_CONFIG_DIRECTORY is defined in the environment
    if "REACH_CONFIG_DIRECTORY" not in os.environ:
        print("REACH_CONFIG_DIRECTORY must be defined in the environment")
        exit()

    # Check if file exists
    full_path = os.path.join(os.environ['REACH_CONFIG_DIRECTORY'], options.config_file)
    if not os.path.exists(full_path) or not os.path.isfile(full_path):
        print(f"Provided file ({options.config_file}, fullpath {full_path}) could not be found or is not a file")
        exit()

    # Load configuration
    c = REACHConfig(options.config_file)

    # Create observation instance
    obs = REACHObservation(c['observation'],
                           c['operations'],
                           c['configuration'] if 'configuration' in c.keys() else None)

    if options.dry_run:
        # Perform a dry run to test the observation config
        elapsed_time = obs.dry_run_operations()
        logging.info(f"Observation should take around {elapsed_time}s to run")
    else:
        # Run operations
        obs.run_observation()
