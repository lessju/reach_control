import logging
import sched
from datetime import datetime
from time import sleep, time

import ephem
import numpy as np
import serial
import serial.tools.list_ports as list_ports
from skrf import Network

from pyreach.core.errors import ReachException


def check_vna_calibration(s11file, measured_pnax, accuracy=5):
    """Takes CMT VNA S11 measurement of VNA test load after VNA calibration as
    Re+Im .s1p file.
    Checks complex impedance within accuracy% of PNA-X measurement at 50 MHz"""

    data = Network(s11file)
    zl = float(data.z_mag[0])

    # Compare with previous measurements
    comparison_mag = np.isclose(a=zl, b=np.real(measured_pnax), rtol=accuracy * 0.01)

    logging.info(f"VNA calibration. Magnitude: {zl:.2f} == {np.real(measured_pnax):.2f}? ")

    assert np.all(comparison_mag), "Receiver VNA test load (magnitude)"


def connect_to_serial_device(serial_number, baudrate):
    """ Connect to a specific device with the provided product ID """

    serial_number = str(serial_number)
    device = None
    for d in list_ports.comports():
        if d.serial_number == serial_number:
            device = d.device

    if device is None:
        raise ReachException(f"Device with serial number {serial_number} not found!")
    else:
        return serial.Serial(device, baudrate)


def get_sidereal_time(longitude, latitude, date_time=None):
    """ Calculate current sidereal time at given location """

    # Create observer at required position
    loc = ephem.Observer()
    loc.lon, loc.lat = longitude, latitude

    # Assign current or provided time
    if date_time is None:
        loc.date = datetime.utcnow()
    else:
        loc.date = datetime.fromtimestamp(date_time)

    # Return sidereal time
    return ephem.degrees(loc.sidereal_time()) / ephem.degree


def schedule_utc(date_time):
    """ Wait for the specified number of seconds to elapse """

    # If date_time is "now", then return immediately
    if date_time < datetime.utcnow():
        return

    # Format input date_time as required
    start_time = datetime.utcfromtimestamp(int(time()))

    # Check for how long we have to wait
    total_seconds = (date_time - start_time).total_seconds()

    # Sanity check
    if total_seconds <= 5:
        logging.warning("Cannot schedule before 5 seconds in the future. Ignoring schedule")
        return

    # Wait for the required duration
    logging.info(f"Operations will start in {int(total_seconds // 60)} minutes {int(total_seconds % 60)} seconds")
    s = sched.scheduler(time, sleep)
    s.enter(total_seconds, 0, lambda: None, [])
    s.run()


def schedule_lst(date_time):
    """ Wait for the specified number of seconds to elapse """

    logging.warning("LST scheduling is not supported yet. Ignoring")


def schedule(date_time, mode):
    """ Wait for specified time, with required mode """
    if mode == "UTC":
        schedule_utc(date_time)
    elif mode == "LST":
        schedule_lst(date_time)
    else:
        logging.error("Scheduling mode {} is not supported, ignoring".format(mode))


def extract_list_from_string(values):
    """ Extract values from string representation of list
    :param values: String representation of values
    :return: List of values
    """
    # Return list
    converted = []
    for item in values.split(","):
        # Check if item contains a semi-colon
        if item.find(":") > 0:
            index = item.find(":")
            lower = item[:index]
            upper = item[index + 1:]
            converted.extend(list(range(int(lower), int(upper))))
        else:
            converted.append(int(item))
    return converted
