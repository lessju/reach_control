class ReachException(Exception):
    """ Base class for other exceptions """
    pass


class ObservationsConflictException(ReachException):
    """ Scheduling error, new observation conflicts with already scheduled ones """
    pass


class ObservationInThePastException(ReachException):
    """ Scheduling error, new observation start time is in the past """
    pass


class ObservationDoesNotExistException(ReachException):
    """ Scheduling error, specified observation does not exist"""
    pass


class UControllerException(Exception):
    """ Exception related to uController """
    pass


class VNAException(ReachException):
    """ Exception related to VNA """
    pass


class PDUException(ReachException):
    """ Exceptions related to PDU """
    pass
