#!/usr/bin/env python
import os
import logging
import datetime

from slack_sdk import WebClient

from pyreach.reach_config import REACHConfig


class ReachSlack:

    def __init__(self, token_file, secret_file=None, channel="reach-notifications"):
        """ Initialise slack instance """

        self._slack_channel = channel
        self._token = self._get_token(token_file)
        self._secret = self._get_secret(secret_file)

        if self._token is None:
            return

        try:
            self._slack = WebClient(token=self._token)
        except:
            logging.warning(f"Slack: Not a valid token {self._token}. Not posting on slack")

    def _send_message(self, message):
        """ Send a message to the Slack channel """
        if self._slack is None:
            return

        try:
            msg = datetime.datetime.strftime(datetime.datetime.utcnow(), "%Y-%m-%d %H:%M:%S  " + message)
            self._slack.chat_postMessage(channel=self._slack_channel, text=msg)
        except:
            logging.warning(f"Slack Exception: Channel: {self._slack_channel}, message: {message}, Token: {self._token}")

    def send_message(self, message):
        """ Send a message to the Slack channel, as is"""
        self._send_message(message=message)

    def info(self, message=""):
        self._send_message(message=f" - INFO - {message}")

    def warning(self, message="", v=False):
        self._send_message(message=f" - WARNING - {message}")

    def error(self, message="", v=False):
        self._send_message(message=f" - ERROR - {message}")

    @property
    def slack_token(self):
        return self._token

    @property
    def slack_secret(self):
        return self._secret

    @staticmethod
    def _get_token(token_file):
        """ Get token """

        if os.path.exists(token_file):
            with open(token_file) as f:
                tok = f.readline()

            if tok[-1] == "\n":
                tok = tok[:-1]
            return tok
        else:
            logging.warning(f"Invalid token file '{token_file}'. Not posting on slack")
            return None

    @staticmethod
    def _get_secret(secret_file):
        """ Get signing secret """

        if secret_file is None:
            return None

        if os.path.exists(secret_file):
            with open(secret_file) as f:
                tok = f.readline()

            if tok[-1] == "\n":
                tok = tok[:-1]
            return tok
        else:
            logging.warning(f"Invalid secret file '{secret_file}'. Not posting on slack")
            return None


class DummySlack(ReachSlack):
    """ Dummy slack instance to avoid help with instance checking """

    def __init__(self):
        return

    def info(self, m="", v=False):
        pass

    def warning(self, m="", v=False):
        pass

    def error(self, m="", v=False):
        pass


if __name__ == "__main__":
    token = REACHConfig()['software_framework']['slack_token_file']
    client = ReachSlack(token)
    client.info("Testing REACH bot")
