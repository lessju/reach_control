from enum import Enum

import pytz
from bson.objectid import ObjectId
from mongoengine import *
import datetime

from pyreach.instrument.devices import Device
from pyreach.reach_config import REACHConfig


class ObservationStatus(Enum):
    PENDING = 1
    RUNNING = 2
    FINISHED = 3
    FAILED = 4
    NOT_SCHEDULED = 5


class ObservationType(Enum):
    SKY = 1
    CALIBRATION = 2


class Observation(Document):
    _id = ObjectIdField(required=True, default=ObjectId, primary_key=True)
    observation_type = EnumField(ObservationType, required=True)
    observation_name = StringField(default="Default observation")
    created_at = DateTimeField(required=True, default=datetime.datetime.utcnow)
    status = EnumField(ObservationStatus, default=ObservationStatus.PENDING)
    start_time = DateTimeField(required=True)
    stop_time = DateTimeField(required=True)
    configuration = StringField(required=True)

    @queryset_manager
    def get(self, query_set, from_time=None, to_time=None, status=None):
        query = Q()

        if from_time:
            query &= Q(start_time__gte=from_time)

        if to_time:
            query &= Q(start_time__lte=to_time)

        if status:
            query &= Q(status__exact=status)

        return query_set.filter(query)


class TPMStatus(Document):
    _id = ObjectIdField(required=True, default=ObjectId, primary_key=True)
    receiver_id = StringField(required=True)
    date_time = DateTimeField(required=True, default=datetime.datetime.utcnow)
    board_temperature = FloatField()
    fpga1_temperature = FloatField()
    fpga2_temperature = FloatField()
    voltage = FloatField()


class MonitoringProbe(Document):
    _id = ObjectIdField(required=True, default=ObjectId, primary_key=True)
    device = EnumField(Device, required=True)
    sensor_index = IntField(required=True, default=0)
    location = StringField()
    description = StringField()


class Temperature(Document):
    _id = ObjectIdField(required=True, default=ObjectId, primary_key=True)
    date_time = DateTimeField(required=True, default=datetime.datetime.utcnow)
    probe = ReferenceField(MonitoringProbe)
    temperature = FloatField(required=True)
    meta = {'indexes': ['-date_time']}


class Power(Document):
    _id = ObjectIdField(required=True, default=ObjectId, primary_key=True)
    date_time = DateTimeField(required=True, default=datetime.datetime.utcnow)
    probe = ReferenceField(MonitoringProbe)
    power = FloatField(required=True)
    meta = {'indexes': ['-date_time']}


def get_pending_observations():
    """ Return a list of pending observations"""
    return Observation.get(status=ObservationStatus.PENDING).order_by('+start_time')


def get_running_observation():
    """ Return the current running observation """
    return Observation.get(status=ObservationStatus.RUNNING)


def check_observation_overlap(start_time, end_time):
    """ Check whether there are observations which should run within the current pending observations """

    # Go through all pending observations and check whether there are overlaps
    for observation in get_pending_observations():
        so_start = observation.start_time.replace(tzinfo=pytz.utc) - datetime.timedelta(minutes=1)
        so_end = observation.stop_time.replace(tzinfo=pytz.utc) + datetime.timedelta(minutes=1)

        if (start_time <= so_end) and (so_start <= end_time):
            return True

    return False


def get_probe_foreign_keys():
    devices = {}
    for item in MonitoringProbe.objects():
        key = (item.device, item.sensor_index)
        if key not in devices.keys():
            devices[key] = item._id

    return devices


if __name__ == "__main__":
    REACHConfig()
    print(get_probe_foreign_keys())


