import redis

from pyreach.core.singleton import Singleton


@Singleton
class RedisManager:
    def __init__(self, host='127.0.0.1', port=6379):
        self._connection_pool = redis.ConnectionPool(host=host, port=port)
        self.redis = redis.Redis(connection_pool=self._connection_pool)


# The message broker we will be using
broker = RedisManager.Instance().redis

# The PubSub interface of the redis instance
pub_sub = broker.pubsub()


class Channels:
    """ Encapsulate the channels that are available """

    # Channel is used to push event messages to slack
    NOTIFICATIONS_SLACK = 'reach__slack_notifications'

    # Channel is used to push the system status to the front end
    SYSTEM_CTL = 'reach__system_status'
