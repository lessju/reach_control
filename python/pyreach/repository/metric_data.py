import datetime

from pyreach.reach_config import REACHConfig
from pyreach.repository.models import Temperature, get_probe_foreign_keys, TPMStatus, MonitoringProbe
from pyreach.instrument.devices import Device

class MetricData:
    def __init__(self):
        pass

    @staticmethod
    def get_receiver_temperatures(start_date, stop_date=None, devices=None):
        """ Get receiver temperatures from database
        @param start_date: The start date from when to get values
        @param stop_date: The stop date at which to stop getting values
        @param devices: The list of devices to get temperatures of """

        # Compute stop date
        if stop_date is None:
            stop_date = datetime.datetime.utcnow()

        # If devices is None, return all of them
        if devices is None:
            # TEMPORARY: Adaptive Junior is not enabled
            # devices = [(Device.ADAPTIVE_JUNIOR, 0), (Device.TEC_TCM, 0)]
            devices = [(Device.TEC_TCM, 0)]
            for i, probe in enumerate(REACHConfig()['tc08_temperature_sensor']['channel_names']):
                devices += [(Device.TC08, i)]

            for i, probe in enumerate(REACHConfig()['ucontroller']['temperature']):
                devices += [(Device.TC08, i)]

        # Sanity check on devices
        if type(devices) is not list:
            devices = list(devices)

        # Get list of foreign keys and filter depending on what's required
        device_foreign_keys = get_probe_foreign_keys()

        return_values = {}
        for device in devices:
            temps = Temperature.objects.filter(date_time__gte=start_date, date_time__lte=stop_date,
                                               probe=device_foreign_keys[device])
            temps.order_by("date_time")
            return_values[device] = [(t.date_time, t.temperature) for t in temps]

        return return_values

    @staticmethod
    def get_tpm_temperatures(start_date, stop_date=None):
        """ Get TPM temperatures from database
        @param start_date: The start date from when to get values
        @param stop_date: The stop date at which to stop getting values  """

        # Compute stop date
        if stop_date is None:
            stop_date = datetime.datetime.utcnow()

        temps = TPMStatus.objects.filter(date_time__gte=start_date, date_time__lte=stop_date)
        temps.order_by("date_time")
        return [(t.date_time, t.board_temperature, t.fpga1_temperature, t.fpga2_temperature) for t in temps]

    @staticmethod
    def get_latest_temperatures():
        """ Get the latest temperature for all the available probes """

        # Generate list of devices to get temperature data for
        devices = [(Device.ADAPTIVE_JUNIOR, 0), (Device.TEC_TCM, 0)]
        for i, probe in enumerate(REACHConfig()['tc08_temperature_sensor']['channel_names']):
            devices += [(Device.TC08, i)]

        for i, probe in enumerate(REACHConfig()['ucontroller']['temperature']):
            devices += [(Device.TC08, i)]

        # Get device foreign keys
        device_keys = get_probe_foreign_keys()

        db_values = {}
        for device in devices:
            temps = Temperature.objects.filter(probe=device_keys[device]).order_by('-date_time').first()
            db_values[device] = temps

        # Generate user-readable dictionary of values
        return_values = {'Adaptive Junior': (f"{db_values[(Device.ADAPTIVE_JUNIOR, 0)].temperature:.1f} °C",
                                             db_values[(Device.ADAPTIVE_JUNIOR, 0)].date_time.strftime("%H:%M:%S")),
                         'TEC TCM': (f"{db_values[(Device.TEC_TCM, 0)].temperature:.3f} °C",
                                     db_values[(Device.TEC_TCM, 0)].date_time.strftime("%H:%M:%S"))}

        for i, probe in enumerate(REACHConfig()['tc08_temperature_sensor']['channel_names']):
            return_values[f'TC08 - {list(probe.keys())[0]}'] = (f"{db_values[(Device.TC08, i)].temperature:.1f} °C",
                                                                db_values[(Device.TC08, i)].date_time.strftime("%H:%M:%S"))

        for i, probe in enumerate(REACHConfig()['ucontroller']['temperature']):
            try:
                return_values[f'Ucontroller - {probe}'] = (f"{db_values[(Device.UCONTROLLER, i)].temperature:.1f} °C",
                                                           db_values[(Device.UCONTROLLER, i)].date_time.strftime("%H:%M:%S"))
            except:
                continue

        return return_values
    @staticmethod
    def get_latest_tpm_status():
        """ Get the latest TPM status from database """
        vals = TPMStatus.objects.order_by('-date_time').first()
        if len(vals) == 0:
            return None

        return {"Board Temperature: ": f"{vals.board_temperature:.1f} °C",
                "Datetime": vals.date_time.strftime("%d/%m/%Y %H:%M:%S"),
                "FPGA1 Temperature": f"{vals.fpga1_temperature:.1f} °C",
                "FPGA2 Temperature": f"{vals.fpga2_temperature:.1f} °C",
                "Voltage": f"{vals.voltage:.1f} V"}


if __name__ == "__main__":
    from pyreach.instrument.devices import Device

    REACHConfig()
    s = MetricData()

    ret = s.get_receiver_temperatures(start_date=datetime.datetime.utcnow() - datetime.timedelta(hours=24))

    import plotly.express as px
    import plotly.graph_objects as go
    import numpy as np

    fig = go.Figure()
    for k, v in ret.items():
        if k == (Device.ADAPTIVE_JUNIOR, 0):
            continue
        data = np.array(ret[k])
        # fig = px.line(x=data[:,0], y=data[:,1])
        fig.add_trace(go.Scatter(x=data[:, 0], y=data[:, 1], mode="lines", name=str(k)))
    fig.write_html("test.html")