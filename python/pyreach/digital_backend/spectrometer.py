#! /usr/bin/env python

import numpy as np
import logging
import os

from pyreach.digital_backend.tile_reach import Tile
from pyreach.digital_backend.spectra import Spectra
from pyreach.reach_config import REACHConfig

__author__ = 'Alessio Magro'


class Spectrometer:

    def __init__(self, ip, port, lmc_ip, lmc_port,
                 enable_spectra=True, sampling_rate=400e6,
                 enabled_input=None):
        """ Class which interfaces with TPM and spectrometer firmware """

        # Create Tile
        if enabled_input is None:
            enabled_input = [0, 16]

        adc0 = enabled_input[0] // 2
        adc1 = enabled_input[1] // 2
        adc_channel0 = enabled_input[0] % 2
        adc_channel1 = enabled_input[1] % 2
        self._tile = Tile(ip=ip, port=port, lmc_ip=lmc_ip, lmc_port=lmc_port,
                          sampling_rate=sampling_rate,
                          enabled_adc=[adc0, adc1], adc_channel=[adc_channel0, adc_channel1])

        # Create and initialise receiver
        self._spectra = None
        if enable_spectra:
            self._spectra = Spectra(ip=lmc_ip, port=lmc_port)
            self._spectra.initialise()

    def connect(self):
        """ Connect to TPM """
        self._tile.connect()

    def program(self, bitfile):
        """ Download a firmware to the TPM
        @param bitfile: Filepath to bitstream """

        if os.path.exists(bitfile) and os.path.isfile(bitfile):
            self._tile.program_fpgas(bitfile=bitfile)
        else:
            logging.error("Could not load bitfile %s, check filepath" % bitfile)

    def program_cpld(self, bitfile):
        """ Update CPLD firmware on TPM
        @param bitfile: Path to bitstream """

        if os.path.exists(bitfile) and os.path.isfile(bitfile):
            logging.info("Using CPLD bitfile {}".format(bitfile))
            self._tile.program_cpld(bitfile)
        else:
            logging.error("Could not load bitfile {}, check filepath".format(bitfile))

    def initialise(self, channel_truncation=2, integration_time=1, ada_gain=None):
        """ Initialise the TPM and spectrometer firmware """

        logging.info("Initialising TPM")
        self._tile.initialise(enable_ada=True if ada_gain is not None else False)

        # Set ada gain if enabled
        if ada_gain is not None:
            self._tile.tpm.tpm_ada.set_ada_gain(ada_gain)

        logging.info("Using 1G for LMC traffic")
        self._tile.set_lmc_download("1g")
        self._tile.set_lmc_integrated_download("1g", 1024, 1024)

        # Set channeliser truncation
        logging.info("Configuring channeliser")
        self._tile.set_channeliser_truncation(channel_truncation)

        # Configure continuous transmission of integrated channel
        self._tile.stop_integrated_data()
        self._tile.configure_integrated_channel_data(integration_time)

        # Perform synchronisation
        self._tile.post_synchronisation()

        logging.info("Setting data acquisition")
        self._tile.start_acquisition()

        self._tile.download_polyfilter_coeffs("hann")
        self._tile['board.regfile.ethernet_pause'] = 8000

    def reset(self):
        """ Reset spectrometer """
        self._tile.reset_board()

    def acquire_spectrum(self, channel=0, nof_seconds=1, wait_seconds=0, integrate=True):
        """ Acquire spectra for defined number of seconds """

        if self._spectra is None:
            logging.warning("Cannot acquire spectra. Acquisition not initialised")
            return None

        # Start receiver
        self._spectra.start_receiver(nof_seconds, wait_seconds)

        # Wait for receiver to finish
        # Spectra will be received in spectra/signals/channel order
        timestamps, spectra = self._spectra.wait_for_receiver()

        # Return spectra
        if integrate:
            spectra = np.sum(spectra, axis=0)
            return timestamps[0], spectra[channel]
        else:
            return timestamps, spectra[:, channel, :]

    def is_programmed(self):
        """ Check whether the TPM is programmed """
        return self._tile.tpm.is_programmed()

    def get_board_temperature(self):
        """ Read board temperature """
        return self._tile.get_temperature()

    def get_voltage(self):
        """ Read board voltage """
        return self._tile.get_voltage()

    def get_fpga1_temperature(self):
        """ Get FPGA1 temperature """
        return self._tile.get_fpga0_temperature()

    def get_fpga2_temperature(self):
        """ Get FPGA2 temperature """
        return self._tile.get_fpga1_temperature()


if __name__ == "__main__":

    # Use OptionParse to get command-line arguments
    from optparse import OptionParser
    from sys import argv, stdout

    parser = OptionParser(usage="usage: %spectrometr [options]")
    parser.add_option("-f", "--bitfile", action="store", dest="bitfile",
                      default=None, help="Bitfile to use (-P still required)")
    parser.add_option("-P", "--program", action="store_true", dest="program",
                      default=False, help="Program FPGAs [default: False]")
    parser.add_option("-C", "--program-cpld", action="store_true", dest="program_cpld",
                      default=False, help="Program CPLD (cannot be used with other options) [default: False]")
    parser.add_option("-I", "--initialise", action="store_true", dest="initialise",
                      default=False, help="Initialise TPM [default: False]")
    parser.add_option("-R", "--reset", action="store_true", dest="reset",
                      default=False, help="Reset TPM, will need to be re-programmed and initialised [default: False]")
    parser.add_option("-S", "--enable-spectra", action="store_true", dest="spectra",
                      default=False, help="Enable acqusition of spectra on this connection [default: False]")
    parser.add_option("--sampling_rate", action="store", dest="sampling_rate",
                      default="400", help="Sampling rate in MHz [default: 400, available: 400, 700, 800, 1000]")
    parser.add_option("--input0", action="store", dest="input0",
                      default="0", help="First input [default: 0, available: [0: 15]]")
    parser.add_option("--input1", action="store", dest="input1",
                      default="16", help="Second input [default: 16, available: [16: 31]]")
    (command_line_args, args) = parser.parse_args(argv[1:])

    # Initialise REACH config
    conf = REACHConfig()['spectrometer']

    # Create tile instance
    tile = Spectrometer(conf['ip'], int(conf['port']), conf['lmc_ip'], int(conf['lmc_port']),
                        enable_spectra=command_line_args.spectra,
                        sampling_rate=int(command_line_args.sampling_rate)*1e6,
                        # Enabled ADCs (one per FPGA)
                        # The first enabled ADC must be in the range [0:15]
                        # the second enabled ADC must be in the range [16:31]
                        enabled_input=[int(command_line_args.input0), int(command_line_args.input1)]
                        )

    # Program CPLD
    if command_line_args.program_cpld:
        logging.info("Programming CPLD")
        tile.program_cpld(command_line_args.bitfile)

    # Program FPGAs if required
    if command_line_args.program:
        logging.info("Programming FPGAs")
        tile.program(os.path.join(os.path.expanduser(os.environ['REACH_CONFIG_DIRECTORY']), conf['bitstream']))

    # Initialise TPM if required
    if command_line_args.initialise:
        tile.initialise(int(conf['channel_truncation']),
                        float(conf['integration_time']),
                        int(conf['ada_gain']))

    # Connect to board
    tile.connect()

    # Reset board if required
    if command_line_args.reset:
        tile.reset()

