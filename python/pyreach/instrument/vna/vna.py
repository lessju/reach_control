import os.path
import getpass
from subprocess import Popen
from shutil import copy2
from typing import Union
import numpy as np
import logging
import psutil
import time

import pyvisa.errors

from pyreach.instrument.ucontroller.microcontroller import Microcontroller
from pyreach.core.errors import VNAException
from pyreach.instrument.vna.scpi_interface import SCPIInterface
from pyreach.reach_config import REACHConfig


class VNA:
    """ Interface class for Copper Mountain VNA TR1300 """

    def __init__(self, gui_path,
                 save_directory=None,
                 copy_directory=None,
                 term="\n"):
        """ Class constructor """

        # Create VISA interface
        self.term = term

        # Connect to VNA
        self._gui_process = None
        self.initialise(gui_path)

        # Sanity check on directories
        self._save_directory = None
        if save_directory is not None:
            save_directory = os.path.expanduser(save_directory).replace("$USER", getpass.getuser())
            if not os.path.exists(save_directory) or not os.path.isdir(save_directory):
                logging.error(f"Provided VNA save directory {save_directory} does not exist, ignoring")
            else:
                self._save_directory = save_directory

        self._copy_directory = None
        if copy_directory is not None:
            copy_directory = os.path.expanduser(copy_directory).replace("$HOME", getpass.getuser())
            if not os.path.exists(copy_directory) or not os.path.isdir(copy_directory):
                logging.error(f"Provided VNA save directory {copy_directory} does not exist, ignoring")
            else:
                self._copy_directory = copy_directory

        # Define resolution steps
        self._resolution_steps = [1, 3, 10, 30, 100, 300, 1000, 3000, 10000, 30000]

        # Define calibration standards
        self._calibration_standards = ['OPEN', 'SHORT', 'LOAD', 'THRU', 'OFF', 'ON', 'APPLY',
                                       'SLOAD', 'ARBI', 'DATABASED', None]

        # Define state save types
        self._save_type = ['state', 'cstate', 'dstate', 'cdstate', ]

        # Settings placeholder
        self._average = 20

    def __del__(self):
        """ Destructor """
        self.terminate()

    def initialise(self, gui_path):
        """ Connect to and load VNA """

        # Wait for VNA to load, try a number of times

        # Next tries include a power reset
        for i in range(3):

            # Power reset VNA
            self._reset_vna_power()

            # GUI executable must be running to communicate with the VNA
            if i == 0:
                self._gui_process = self._run_gui(gui_path)

            # Check if now the VNA is loaded
            if self._load_VNA():
                break

            logging.warning(f"Could not load VNA, power cycling the device and trying again [attempt {i+2}]")

        if self._interface is None:
            logging.critical('Could not load VNA!', exc_info=True)
            raise VNAException("Could not load VNA!")

        # Hide GUI
        # self.write("SYST:HIDE")

    def _load_VNA(self):
        """ Wait for VNA to load, and return status """
        try:
            self._interface = SCPIInterface()
        except VNAException:
            self._interface = None
            return False

        # Wait for VNA to load
        count = 0
        while True:
            try:
                if self.read("SYST:READy?") == "1":
                    return True
                else:
                    time.sleep(2)
            except pyvisa.errors.VisaIOError:
                pass

            count += 1
            if count == 5:
                return False

    def terminate(self):
        """ Terminate VNA """

        # Check if _gui_process attribute exists
        if self._gui_process is None:
            return

        # Instruct VNA user interface to shut down and close connection
        try:
            # self.write("SYST:TERM")
            # time.sleep(2)
            self._interface.close()
        except (BrokenPipeError, AttributeError):
            pass

        # Kill VNA GUI if launched from this object
        self._gui_process.kill()
        if self._gui_process.wait(1) is None:
            self._gui_process.terminate()
        self._gui_process = None

    def configure(self, channel=1, start_frequency=40, stop_frequency=180,
                   ifbw=1000, average=20, calibration_kit=23, power_level=-5,
                   nof_points=2001, enable_debug=False):
        """ VNA Initialization
        @param channel: Input channel
        @param start_frequency: Start frequency in MHz
        @param stop_frequency: Stop frequency in MHz
        @param ifbw: Intermediate frequency bandwidth
        @param average: Average
        @param power_level: Power level in dBm
        @param calibration_kit: Calibration kit as defined in VNA GUI """

        # First set trigger source to be internal (stop continuously acquiring)
        self.set_trigger_source(internal=False)

        # Set parameters
        self.channel(channel)
        self.freq(start=start_frequency, stop=stop_frequency)
        self.ifbw(ifbw)
        self.average(average)
        self.calibration_kit(calibration_kit)
        self.power_level(power_level)

        self.write(f"SENS:SWE:POIN {nof_points}")

        if enable_debug:
            self.write("CALC:FORM SLOG")
            logging.debug(f'Enabled debug')

        return True

    def write(self, cmd: str):
        """ Write command to VNA """
        self._interface.write(cmd + self.term)

    def read(self, cmd: str, delay: float = 0.1):
        """ Read reply from VNA """
        return self._interface.read(cmd + self.term, delay)

    def freq(self, start: Union[None, int] = None, stop: Union[None, int] = None):
        """ Set start and stop frequency in MHz
            If no parameters provided, this method returns current frequency

            The corresponding SCPI commands are:
            SENSe<Ch>:FREQuency:STARt <frequency>
            SENSe<Ch>:FREQuency:STOP <frequency>
        """

        # Check if start frequency is lower than stop frequency
        if start >= stop:
            logging.error(f"VNA frequency start {start} must be less than stop {stop}")
            return

        if start is None and stop is None:
            start = self.read('SENS1:FREQ:STAR?').strip()
            stop = self.read('SENS1:FREQ:STOP?').strip()
            return float(start), float(stop)

        self.write(f'SENS1:FREQ:STAR {start} MHZ')
        logging.debug(f'Set start frequency to {start} MHz')
        self.write(f'SENS1:FREQ:STOP {stop} MHZ')
        logging.debug(f'Set stop frequency to {stop} MHz')

    def ifbw(self, resolution=None):
        """ Set IF bandwidth resolution
        @param resolution: Frequency resolution. If not set the current ifbw is returned

        The corresponding SCPI command is:
        SENSe<Ch>:BWIDth[:RESolution] <frequency>
        available options are:
            10, 30, 100, 300, 1000, 3000, 10000, 30000
        """

        if resolution not in self._resolution_steps:
            logging.error(f"VNA resolution {resolution} not recognised. Ignoring")
            return

        if resolution:
            self.write(f'SENS1:BWID {resolution} HZ')
            logging.debug(f'Set IF bandwidth to {resolution}')
        else:
            self.read('SENS1:BWID?')

    def channel(self, channel=1):
        """ Set input channel
        DISPlay:WINDow<Ch>:ACTivate
        Sets the active channel (no query)
        """

        self.write(f'DISP:WIND{channel}:ACT')

    def calibration_kit(self, kit=15):
        """
        SENSe<cnum>:CORRection:COLLect:CKIT[:SELect] <numeric>
        MMEMory:LOAD:CKIT<Ck> <string>
        """
        self.write(f'SENS1:CORR:COLL:CKIT {kit}')
        kit_used = self.read(f'SENS1:CORR:COLL:CKIT:LAB?')
        logging.info(f'Calibration kit #{kit} selected ({kit_used})')

    def set_trigger_source(self, internal=False):
        """ Set trigger source """
        if internal:
            self.write("TRIG:SOURCE INT")
        else:
            self.write("TRIG:SOURCE BUS")

    def calibrate(self, source):
        """ Calculate calibration data and apply """

        source = source.upper()
        if source not in self._calibration_standards:
            logging.error(f"Invalid calibration source: {source}")
            return

        # Sets trigger to internal
        self.write("TRIG:SOUR INT")

        # Measure short
        if source == "SHORT":
            self.write("SENS:CORR:COLL:SHOR 1")
            self.wait()

        # Measure open
        elif source == "OPEN":
            self.write("SENS:CORR:COLL:OPEN 1")
            self.wait()

        # Measure load
        elif source == "LOAD":
            self.write("SENS:CORR:COLL:LOAD 1")
            self.wait()

        # Save calibration
        elif source == "APPLY":
            self.write("SENS:CORR:COLL:METH:SOLT1 1")
            self.write("SENS:CORR:COLL:SAVE")
            self.wait()

        else:
            raise NotImplementedError(f"VNA calibration source {source} not supported")

    def state_save(self, filename, save_type="CSTate"):
        """ Save VNA state
        MMEMory:STORe:STYPe {STATe|CSTate|DSTate|CDSTate}
            STATe       Measurement conditions
            CSTate      Measurement conditions and calibration tables
            DSTate      Measurement conditions and data traces
            CDSTate     Measurement conditions, calibration tables and data
                        traces

        MMEMory:STORe[:STATe] <string>
        """

        if save_type.lower() not in self._save_type:
            logging.error(f"VNA save state type {self._save_type} not recognised. Ignoring")
            return

        self.write(f'MMEM:STOR:STYP {save_type}')
        self.write(f'MMEM:STOR "{filename}"')

        # Wait for file to be written
        time.sleep(0.1)

        # Copy generated file to copy directory
        if self._copy_directory is not None and self._save_directory is not None:
            copy2(os.path.join(*[self._save_directory, 'State', f'{filename}.cfg']), self._copy_directory)

        logging.debug('Save system state of type {} to file {}'.format(save_type, filename))

    def state_recall(self, filename):
        """ Load VNA state
        MMEMory:LOAD[:STATe] <string>
        """
        self.write(f'gMMEM:LOAD {filename}')
        logging.debug(f'Load system state from file {filename}')

    def trace(self, s11='MLOG', s21='MLOG', res=1001):
        """ Perform a trace """

        # Set up 2 traces, S11, S21
        self.write('CALC1:PAR:COUN 2')  # 2 Traces
        self.write('CALC1:PAR1:DEF S11')  # Choose S11 for trace 1
        self.write(f'CALC1:TRAC1:FORM {s11}')  # log Mag format

        # Format can be SMIT or POL or SWR and many other types
        self.write('CALC1:PAR2:DEF S21')  # Choose S21 for trace 2
        self.write(f'CALC1:TRAC2:FORM {s21}')  # Log Mag format
        self.write('DISP:WIND1:TRAC2:Y:RPOS 1')  # Move S21 up
        self.write(f'SENS1:SWE:POIN {res}')  # Number of points

    def snp_save(self, filename, save_format="ri"):
        """ Save measurement in touchstone file

        MMEMory:STORe:SNP:FORMat {RI|DB|MA}
            " MA" Logarithmic Magnitude / Angle format
            " DB" Linear Magnitude / Angle format
            " RI" Real part /Imaginary part format
        MMEMory:STORe:SNP[:DATA] <string>
            Saves the measured S-parameters of the active channel into a
            Touchstone file. The file type (1-port or 2-port) is set by the
            MMEM:STOR:SNP:TYPE:S1P and MMEM:STOR:SNP:TYPE:S2P
            commands. 1-port type file saves one reflection parameter: S11 or
            S22. 2-port type file saves all the four parameters: S11, S21, S12,
            S22. (no query)
        """

        if save_format not in ['ri', 'db', 'ma']:
            logging.error(f"VNA save format {save_format} not recognised. Ignoring")
            return

        self.write('MMEM:STOR:SNP:TYPE:S1P 1')
        self.write(f'MMEM:STOR:SNP:FORM {save_format}')
        self.write(f'MMEM:STOR:SNP "{filename}"')
        logging.info(f'Saved touchstone file in {save_format} format at {filename}')

        # Wait for file to be written
        time.sleep(0.1)

        # Copy generated file to copy directory
        if self._copy_directory is not None and self._save_directory is not None:
            copy2(f"{self._save_directory}/Touchstone/{filename}.s1p", self._copy_directory)

    def power_level(self, dbm=None):
        """
        SOURce<Ch>:POWer[:LEVel][:IMMediate][:AMPLitude] <power>
            <power>     the power level from -55 to +3
                        Resolution 0.05
        """

        if dbm is None:
            return self.read('SOUR1:POW?')  # Get data as string

        elif 3 >= dbm >= -55:
            rounded_dbm = 0.05 * round(dbm / 0.05)
            self.write(f'SOUR1:POW {rounded_dbm}')
            logging.debug(f'Set power level to {rounded_dbm}')
        else:
            logging.error(f"VNA specified power level {dbm} invalid, must be <=3 and >= -55. Ignoring")

    def measure(self):
        """ Trigger a VNA measurement """

        # Trigger a measurement
        self.write('TRIG:SEQ:SING')  # Trigger a single sweep
        self.wait()
        frequency = self.read('SENS1:FREQ:DATA?')  # Get data as string
        s11 = self.read('CALC1:TRAC1:DATA:FDAT?')  # Get data as string
        s21 = self.read('CALC1:TRAC2:DATA:FDAT?')  # Get data as string

        # Split the long strings into a string list
        # also take every other value from magnitudes since second value is 0
        # If complex data were needed we would use polar format and the second
        # value would be the imaginary part
        frequency = frequency.split(',')
        s11 = s11.split(',')
        s21 = s21.split(',')

        # Change the string values into numbers
        s11 = np.asarray([float(s) for s in s11])
        s21 = np.asarray([float(s) for s in s21])
        s11 = s11[::2] + 1j * s11[1::2]
        s21 = s21[::2] + 1j * s21[1::2]
        frequency = [float(f) / 1e6 for f in frequency]

        return np.vstack((frequency, s11, s21)).T

    def wait(self):
        """ Wait for measurement to complete """
        self.read('*OPC?', delay=0)

    def average(self, count=None):
        """
        SENSe<Ch>:AVERage[:STATe] {ON|OFF|1|0}
        SENSe<Ch>:AVERage[:STATe]?
        SENSe<Ch>:AVERage:COUNt <numeric>
        SENSe<Ch>:AVERage:COUNt?
        """

        # Save in instance
        self._average = count

        if count is None:
            if self.read('SENS1:AVER?') == '0':
                return 0
            else:
                ret = self.read('SENS1:AVER:COUN?')
                return int(ret)
        else:
            if count == 0:
                self.write('SENS1:AVER 0')
                logging.debug('Disable average')
            else:
                self.write('SENS1:AVER 1')
                self.write(f'SENS1:AVER:COUN {count}')
                logging.debug(f'Set average to {count}')

    def sweep(self, nof_points=None, sweep_time=None, sweep_type=None):
        """ Perform a sweep
        SENSe<Ch>:SWEep:POINts <numeric>
        SENSe<Ch>:SWEep:POINts?
        SENSe<Ch>:SWEep:POINt:TIME <time>
        SENSe<Ch>:SWEep:POINt:TIME?
        SENSe<Ch>:SWEep:TYPE {LINear|LOGarithmic|SEGMent|POWer|VVM}
        SENSe<Ch>:SWEep:TYPE?
        """

        STYPE = ['linear', 'logarithmic', 'segment', 'power', 'vvm']
        if nof_points is None and sweep_time is None and sweep_type is None:
            nof_points = int(self.read('SENS1:SWE:POIN?'))
            sweep_time = float(self.read('SENS1:SWE:POIN:TIME?'))
            sweep_type = self.read('SENS1:SWE:TYPE?')
            return {'points': nof_points, 'time': sweep_time, 'type': sweep_type}
        else:
            if isinstance(nof_points, int):
                self.write(f'SENS1:SWE:POIN {nof_points}')
                logging.debug(f'Set sweep points to {nof_points}')
            if isinstance(sweep_time, int):
                self.write(f'SENS1:SWE:POIN:TIME {sweep_time}')
                logging.debug(f'Set sweep time to {sweep_time} second(s)')
            if isinstance(sweep_type, str) and sweep_type.lower() in STYPE:
                self.write(f'SENS1:SWE:TYPE {sweep_type}')
                logging.debug(f'Set sweep type to {sweep_type}')

    def measure_s11(self):
        # Reset sweep number to zero (clear averaging)
        self.write('SENS:AVER:CLE')

        # Get the number of sweeps
        for i in range(0, self._average):
            self.write('TRIG:SING')  # Triggers single sweep
            self.wait()  # Wait for sweep to finish

    @staticmethod
    def _run_gui(gui_path):
        """ Check if GUI is running, and if not launch a process """
        # If there are VNA GUIs running, kill them
        for p in psutil.process_iter():
            if "TRVNA" in p.name():
                try:
                    # Kill the process. NoSuchProcess exception will be raised if the process
                    # no longer exists after the query
                    p.kill()
                except psutil.NoSuchProcess:
                    pass

        if gui_path is None:
            logging.error("VNA GUI is not running and no path provided. Cannot communicate with VNA")
            exit()
        else:
            # Launch GUI is a new process. Argument is to disable signal propagation to new process
            p = Popen([gui_path], preexec_fn=os.setpgrp)

            # Wait for a while
            time.sleep(15)

            return p

    @staticmethod
    def _reset_vna_power():
        """ Reset the VNA's power supply """
        # If VNA is not loaded, we need to power cycle it
        c = REACHConfig()['ucontroller']
        controller = Microcontroller(initialise=False)

        controller.disable_power_source('12v')
        time.sleep(1)
        controller.enable_power_source('12v')

        # Wait for a few second
        time.sleep(15)


if __name__ == "__main__":
    # Initialise configuration
    REACHConfig()

    # Configure and initialise VNA
    conf = REACHConfig()['vna']
    vna = VNA(conf['gui_path'], save_directory=conf['save_directory'], copy_directory="/opt/reach/data")

    logging.info("Connected VNA")
    vna.configure(channel=conf['channel'],
                  start_frequency=conf['start_frequency'],
                  stop_frequency=conf['stop_frequency'],
                  ifbw=conf['ifbw'],
                  average=conf['average'],
                  calibration_kit=conf['calibration_kit'],
                  power_level=conf['power_level'],
                  enable_debug=conf['enable_debug'],
                  nof_points=conf['nof_points'])
    logging.info("Initialised VNA")
    vna.terminate()
