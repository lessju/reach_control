import pyvisa
import pyvisa as visa
import logging

from pyreach.core.errors import VNAException


class SCPIInterface:

    def __init__(self, ip='localhost', port=5025, term='\n', timeout=600000):
        """ SCPI-1999 for Copper Mountain VNA
            Please ensure that you run VNA GUI and enable TCP interface
            before instantiate this class

            ip          default value is localhost
            port        default value is 5025
            term        termination. default value is LF (Line Feed)

            Please refer to the link below for a full list of commands
            https://coppermountaintech.com/wp-content/uploads/2019/08/TRVNA_Programming_Manual_SCPI.pdf
        """
        # Create VISA resource manager instance
        rm = visa.ResourceManager('@py')

        # Connect to VISA device
        try:
            # Create connection to the device
            self.CMT = rm.open_resource('TCPIP0::{}::{}::SOCKET'.format(ip, port))

            # Set termination character
            self.CMT.read_termination = term

            # Set timeout
            self.CMT.timeout = timeout

            # Send an identity query to test connection
            self.read(f'*IDN?{term}')
        except (pyvisa.errors.VisaIOError, ConnectionRefusedError) as e:
            self.CMT.close()
            raise VNAException('Cannot establish connection to VNA')

    def is_connected(self):
        try:
            if self.CMT.session:
                return True
        except pyvisa.errors.InvalidSession:
            pass

        return False

    def write(self, msg, values=None):
        """ Send a message through SCPI A termination is appended for each message """
        if values is None:
            values = []

        self.CMT.write_ascii_values(msg, values)

    def read(self, msg, delay=0.1):
        """ Read value or state through SCPI """
        return self.CMT.query(msg, delay)

    def close(self):
        self.CMT.close()
