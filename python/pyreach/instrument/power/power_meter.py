from pyminicircuits import PowerSensor


class PowerMeter:
    """ Interface with power meter """

    def __init__(self, read_frequency, simulate=False):
        """ Class initializer """
        
        # Initialise power sensor
        self._read_frequency = read_frequency
        self._simulate = simulate

        if not self._simulate:
            self._sensor = PowerSensor()

    def close(self):
        if not self._simulate:
            del self._sensor

    def measure_power(self):
        """ Measure current power """

        # If simulating, return a fixed value
        if self._simulate:
            return 2
        else:
            return self._sensor.get_power(self._read_frequency)


if __name__ == "__main__":
    from pyreach.reach_config import REACHConfig

    conf = REACHConfig()['power_meter']
    sensor = PowerMeter(float(conf['read_frequency']), simulate=False)
    print(sensor.measure_power())
