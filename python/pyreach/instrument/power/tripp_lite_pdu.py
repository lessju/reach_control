#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import print_function

import logging
import os
import re
import time

import pexpect

from pyreach.core.errors import PDUException
from pyreach.reach_config import REACHConfig
from pyreach.utils import extract_list_from_string


class TrippLitePDU:
    COMMAND_PROMPT = '>> '
    COMMAND_TIMEOUT = 100
    NOF_PORTS = 8

    def __init__(self):
        """ Class initialiser """

        # Get information from YAML file
        pdu_config = REACHConfig()['pdu']

        # Store account information
        self._hostname = pdu_config['ip']
        self._username = pdu_config['username']
        self._password = os.environ['REACH__MONGO_PASSWORD']
        self._port_names = [s.upper() for s in pdu_config['ports']]

        # Reference to telnet session
        self._telnet = None

        # Port status regular expression
        self._status_pattern = re.compile(r" +(?P<port>\d) +(?P<status>[a-zA-Z]+)")

        # Port status
        self._port_info = [{'name': self._port_names[i].upper(), 'status': 'OFF'} for i in range(self.NOF_PORTS)]

        # Connect and get current status
        self.connect()
        self.update_status()

    def connect(self):
        # Login via telnet
        try:
            self._telnet = pexpect.spawn('telnet %s' % self._hostname)
        except:
            raise PDUException("Unable to connect to PDU %s" % self._hostname)

        try:
            self._telnet.expect('login: ', timeout=self.COMMAND_TIMEOUT)
            self._telnet.sendline('%s' % self._username)
            self._telnet.expect('Password: ', timeout=self.COMMAND_TIMEOUT)
            self._telnet.sendline('%s' % self._password)
        except:
            raise PDUException("Unable to login")

    def update_status(self):
        """ Get the status of the PDU loads """

        # Go to load status page
        self.select_options(['1', '5', '1'])
        self._telnet.expect(self.COMMAND_PROMPT, timeout=self.COMMAND_TIMEOUT)

        # Extract results
        results = re.findall(self._status_pattern, self._telnet.before.decode())
        for result in results:
            self._port_info[int(result[0]) - 1]['status'] = result[1].upper()

        # Go back to device page
        self._telnet.sendline('M')

    def cycle(self, port):
        self.disable_port(port)
        time.sleep(2)
        self.enable_port(port)

    def enable_port(self, port):
        """ Turn a port on """

        if type(port) in [str, bytes]:
            port = self.get_port_number_from_name(port)

        if self._port_info[port - 1]['status'] == "ON":
            return

        self.select_options(['1', '5', '1', str(port), '3', 'Y', 'M'])
        self._port_info[port - 1]['status'] = "ON"

    def disable_port(self, port):
        """ Turn a port off """

        if type(port) in [str, bytes]:
            port = self.get_port_number_from_name(port)

        if self._port_info[port - 1]['status'] == "OFF":
            return

        self.select_options(['1', '5', '1', str(port), '3', 'Y', 'M'])
        self._port_info[port - 1]['status'] = "OFF"

    def get_port_status(self, port):
        """ Return the status of the specified port """
        return self._port_info[port - 1]['status']

    def get_port_name(self, port):
        return self._port_info[port - 1]['name']

    def get_port_number_from_name(self, name):
        """ Return the port number for the specified name """
        if name.upper() not in self._port_names:
            logging.warning(f"Provided port name {name} not found")
            return -1
        return self._port_names.index(name) + 1

    def select_options(self, options):
        """ Go through menu options """
        if type(options) != list:
            options = [options]

        for option in options:
            self._telnet.expect(self.COMMAND_PROMPT, timeout=self.COMMAND_TIMEOUT)
            self._telnet.sendline(option)

    def close(self):
        """ Close the connection """
        # Now exit the remote host and close the connection.
        self._telnet.sendline('M')
        self._telnet.expect(self.COMMAND_PROMPT, timeout=self.COMMAND_TIMEOUT)
        self._telnet.sendline('Q')
        self._telnet.close()

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def __repr__(self):
        return f'PDU connect by telnet to {self._username}@{self._hostname}'


if __name__ == "__main__":
    # Use OptionParse to get command-line arguments
    from optparse import OptionParser
    from sys import argv

    parser = OptionParser(usage="usage: %pdu [options]")
    parser.add_option("", "--ports", action="store", dest="ports",
                      type=str, default="1:9", help="PDU ports (default: all)")
    parser.add_option("", "--device", action="store", dest="device", default=None,
                      type=str, help="Perform action on specified device")
    parser.add_option("", "--action", action="store", dest="action",
                      default="status", help="Action tp be performed (status | switch_on | switch_off | reset")

    (config, args) = parser.parse_args(argv[1:])

    REACHConfig()

    # An action must be selected
    if config.action not in ["status", "switch_on", "switch_off", "reset"]:
        logging.error("Invalid action {}. Permitted: status | switch_on | switch_off".format(config.action))
        exit(-1)

    # Create port list
    if config.device is None:
        config.ports = extract_list_from_string(config.ports)
        for port in config.ports:
            if port < 1 or port > TrippLitePDU.NOF_PORTS:
                logging.error("Invalid port {}".format(port))
                exit(-1)

    # Connect to PDU
    try:
        pdu = TrippLitePDU()
    except Exception as e:
        logging.error("Could not connect to PDU with IP {} [{}]".format(config.ip, e))
        exit(-1)

    # Check if device is valid
    if config.device is not None:
        config.device = config.device.upper()
        if config.device not in pdu._port_names:
            logging.error(f"Invalid device {config.device}. Must be on of {', '.join(pdu._port_names)}")
            exit(-1)

    # Check what action needs to be performed
    if config.action == "status":
        if config.device is None:
            for port in config.ports:
                logging.info(f"Port {port} powering {pdu.get_port_name(port)} is {pdu.get_port_status(port)}")
        else:
            port = pdu.get_port_number_from_name(config.device)
            logging.info(f"Port {port} powering {pdu.get_port_name(port)} is {pdu.get_port_status(port)}")

    elif config.action == "switch_on":
        if config.device is None:
            for port in config.ports:
                logging.info("Switching port {} on".format(port))
                pdu.enable_port(port)
                logging.info("Switched port {} on".format(port))
        else:
            logging.info("Switching device {} on".format(config.device))
            pdu.enable_port(config.device)
            logging.info("Switched device {} on".format(config.device))

    elif config.action == "switch_off":
        if config.device is None:
            for port in config.ports:
                logging.info("Switching port {} off".format(port))
                pdu.disable_port(port)
                logging.info("Switched port {} off".format(port))
        else:
            logging.info("Switching device {} off".format(config.device))
            pdu.disable_port(config.device)
            logging.info("Switched device {} off".format(config.device))
    else:
        if config.device is None:
            for port in config.ports:
                logging.info("Resetting port {}".format(port))
                pdu.disable_port(port)
                time.sleep(3)
                pdu.enable_port(port)
                logging.info("Reset port {}".format(port))
        else:
            logging.info("Resetting device {}".format(config.device))
            pdu.disable_port(config.device)
            time.sleep(3)
            pdu.enable_port(config.device)
            logging.info("Reset device {}".format(config.device))
