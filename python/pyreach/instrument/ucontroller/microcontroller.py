#!/usr/bin/env python

import functools
import os
from typing import List
from time import sleep
from math import ceil
import logging
import time

from pyreach.core.errors import UControllerException
from pyreach.core.lockfile import LockFile
from pyreach.core.singleton import singleton
from pyreach.reach_config import REACHConfig

# NOTES:
# - The power supplies are turned on on class initialisation
# - Switches are set to pin 1, and MTX is enabled on class initialisation
# - There are checks in place preventing this class from disabling the 5V power source

# Helper to disallow certain function calls on unconnected tiles
from pyreach.utils import connect_to_serial_device


def critical(f):
    @functools.wraps(f)
    def wrapper(self, *args, **kwargs):
        if 'override_critical' in kwargs.keys() and kwargs['override_critical']:
            del kwargs['override_critical']
            ret_value = f(self, *args, **kwargs)
        else:
            self._lock.acquire()
            ret_value = f(self, *args, **kwargs)
            self._lock.release()
        return ret_value

    return wrapper


@singleton
class Microcontroller:
    """ Class to communicate with the REACH receiver microcontroller
        and enable/disable GPIO pins """

    def __init__(self, term="\n", timeout=0.5, initialise=True):
        """ Class constructor
        @param port: Serial prot
        @param baudrate: Serial connection baudrate 
        @param term: Command terminator
        @param timeout: Connection timeout """

        # Set terminator symbol
        self.term = term

        # Create serial connection
        config = REACHConfig()['ucontroller']
        self._conn = connect_to_serial_device(config['serial_id'], config['baudrate'])
        self._conn.timeout = timeout

        logging.info('Interfacing with ucontroller on {}'.format(self._conn.name))

        # Create system-wide lock
        self._lock = LockFile('ucontroller')

        if not self.is_alive():
            logging.error('Cannot reach to ucontroller')

        # Load switches configuration
        self._power = self._load_power_configuration()
        self._switches = self._load_switches_configuration()
        self._peripherals = self._load_peripherals_configuration()
        self._sources = self._load_sources_configuration()

        # Keep track of last enabled source
        self._last_source = None

        # Switch off all sources and reload switches to update state
        self._switches = self._load_switches_configuration()

        # List of switches to keep on (not automatically disabled when switching sources)
        self._always_on_switches = {}

        # Switch on power sources and switches
        if initialise:
            # Initialise ucontroller functionality
            self._initialise_temperatures()

            # Initialise power and sources
            self.initialise()

    def initialise(self):
        """ Turn on power sources and fan """
        self.disable_all_sources()
        self.enable_power_source()
        self.switch_fan_on()

    def turn_off(self):
        """ Turn off power sources and fan"""
        self.disable_all_sources()
        self.disable_power_source()
        self.switch_fan_off()

    def add_always_on_switch(self, switch, port=1):
        """ Add an entry in always_on_switches """
        if switch not in self._switches.keys():
            logging.warning(f"Unknown switch {switch}, ignoring command to keep it always on")
            return

        self._always_on_switches[switch] = {'port': port}
        self.set_gpio(self._switches[switch]['pins'][port - 1]['pin'], 1)
        self._switches[switch]['pins'][port - 1]['state'] = 'on'

    def remove_always_on_switch(self, switch):
        """ Remove entry from always_on_switches """
        if switch not in self._switches.keys():
            logging.warning(f"Unknown switch {switch}, ignoring command to disable keeping it always on")
            return

        port = self._always_on_switches[switch]['port']
        self.set_gpio(self._switches[switch]['pins'][port - 1]['pin'], 0)
        self._switches[switch]['pins'][port - 1]['state'] = 'off'
        self._always_on_switches.pop(switch)

    def toggle_switches(self, switch=None, on=True):
        """ Enable MSN and MSX switches """

        if switch is None:
            for k, v in self._switches.items():
                self.set_gpio(v['pins'][0]['pin'], 1 if on else 0)
            self.enable_mts()
            return

        if switch in self._switches.keys():
            self.set_gpio(self._switches[switch]['pins'][0]['pin'], 1 if on else 0)
        else:
            logging.error(f"Specified switch {switch} not recognised")

    def enable_source(self, source):
        """ Enable a source"""
        if source not in self._sources.keys():
            logging.error(f"Specified source {source} not recognised")
            return

        # Loop over switches associated to source
        for switch in self._sources[source]:
            params = switch['switch']
            source_ref = self._switches[params['name']]

            # Check if associated switch pin is already on, otherwise turn it on.
            self.set_gpio(source_ref['pins'][params['position'] - 1]['pin'], 1)
            source_ref['pins'][params['position'] - 1]['state'] = 'on'

        logging.info(f"Enabled source {source}")

        # Set as last source
        self._last_source = source

    def disable_source(self, source):
        """ Disable a source """
        if source not in self._sources.keys():
            logging.error(f"Specified source {source} not recognised")
            raise UControllerException(f"Specified source {source} not recognised")

        # Loop over switches associated to source
        for switch in self._sources[source]:
            params = switch['switch']
            source_ref = self._switches[params['name']]

            # Check if associated switch pin is already off, otherwise turn it off.
            if source_ref['pins'][params['position'] - 1]['state'] == 'on':
                self.set_gpio(source_ref['pins'][params['position'] - 1]['pin'], 0)
                source_ref['pins'][params['position'] - 1]['state'] = 'off'

                # If switch is in always_on_switches,set the required port
                if source_ref['name'] in self._always_on_switches.keys():
                    self.set_gpio(source_ref['pins'][self._always_on_switches[source_ref['name']]['port'] - 1]['pin'], 1)
                    source_ref['pins'][self._always_on_switches[source_ref['name']]['port'] - 1]['state'] = 'on'

        logging.info(f"Disabled source {source}")

    def is_source_enabled(self, source):
        """ Checks whether the corresponding pins on the ucontroller for the specified source are enabled """
        if source not in self._sources.keys():
            logging.error(f"Specified source {source} not recognised")
            raise UControllerException(f"Specified source {source} not recognised")

        # Loop over switches associated to source
        for switch in self._sources[source]:
            params = switch['switch']
            source_ref = self._switches[params['name']]

            # Check if associated switch pin is already on, otherwise turn it on.
            if source_ref['pins'][params['position'] - 1]['state'] == 'on':
                if self.get_gpio(source_ref['pins'][params['position'] - 1]['pin']) != 1:
                    return False

        return True

    def switch_to_source(self, source):
        """ Disables previous source and enables next one """
        if source not in self._sources.keys():
            logging.error(f"Specified source {source} not recognised")
            raise UControllerException(f"Specified source {source} not recognised")

        # Disable previous source
        if self._last_source is not None:
            self.disable_source(self._last_source)

        # Enable next source
        self.enable_source(source)

    def enable_port(self, switch, port):
        """ Enable the specified port on the specified switch """
        # Check if switch exists
        if switch not in self._switches.keys():
            logging.error(f"Switch {switch} not recognised")
            raise UControllerException(f"Specified switch {switch} not recognised")

        # Check if port is valid
        if len(self._switches[switch]) < port - 1:
            logging.error(f"Port {port} on switch {switch} is invalid")
            raise UControllerException(f"Port {port} on switch {switch} is invalid")

        # Set the specified port to 1 and the rest to 0
        for i, p in enumerate(self._switches[switch]['pins']):
            if i == port - 1:
                self.set_gpio(p['pin'], 1)
                p['state'] = 'on'
            else:
                self.set_gpio(p['pin'], 0)
                p['state'] = 'off'

    def enable_mts(self, ms2_passthrough=False):
        """ Enable MTS """
        self.set_gpio(self._switches['MTS']['pins'][0]['pin'], 1)
        if ms2_passthrough:
            self.set_gpio(self._switches['MS2']['pins'][0]['pin'], 1)
        else:
            self.set_gpio(self._switches['MS2']['pins'][0]['pin'], 0)

    def disable_mts(self):
        """ Disable MTS """
        self.set_gpio(self._switches['MTS']['pins'][0]['pin'], 0)

    def disable_all_sources(self):
        """ Disable all sources """
        logging.info("Switching off all switch pins")
        for switch in self._switches.values():
            for pin in switch['pins']:
                self.set_gpio(pin['pin'], 0)

    def place_on_standby(self):
        """ Place ucontroller on standby """

        # Keep the 24v and 12v power supplies, and fan, on
        self.enable_power_source('24v')
        self.enable_power_source('12v')
        self.disable_power_source('6v')
        self.disable_power_source('NS')
        self.switch_fan_on()

        # Switch all sources off
        self.disable_all_sources()

        # Set all MS switches to position 1
        for k, v in self._switches.items():
            self.set_gpio(v['pins'][0]['pin'], 1)

    def enable_power_source(self, source=None):
        """ Enable power supplies """

        # Enable power source if it is valid
        if source is not None:
            if source not in self._power.keys():
                logging.warning(f"Cannot enable nonexistent power source {source}")
                return

            self.set_gpio(self._power[source]['pin'], 1)
            self._power[source]['state'] = 'on'
        else:
            # If not power source is selected, switch on all power supplies
            for k, v in self._power.items():
                self.set_gpio(v['pin'], 1)
                v['state'] = 'on'

    def disable_power_source(self, source=None):
        """ Disable power supplies """
        # Enable power source if it is valid
        if source is not None:
            if source not in self._power.keys():
                logging.warning(f"Cannot disable nonexistent power source {source}")
                return
            elif source == '5v':
                logging.warning(f"Cannot disable 5V power source")
                return

            self.set_gpio(self._power[source]['pin'], 0)
            self._power[source]['state'] = 'on'
        else:
            # If not power source is selected, switch on all power supplies
            for k, v in self._power.items():
                if k.upper() == '5V':
                    continue
                self.set_gpio(v['pin'], 0)
                v['state'] = 'off'

        # Switch fan if all power sources are off
        if not any([v['state'] == 'on' for v in self._power.values()]):
            self.switch_fan_off()

    def reset(self):
        """ Switch off all power """
        self.disable_power_source()

        # Switch off all sources
        self.disable_all_sources()

    def switch_fan_on(self):
        """ Switch fan on """
        self.enable_power_source('24v')
        self.set_gpio(self._peripherals['fan']['pin'], 1)

    def switch_fan_off(self):
        """ Switch fan off """
        self.set_gpio(self._peripherals['fan']['pin'], 0)

    @critical
    def is_alive(self):
        """ Check whether firmware is running on ucontroller """
        self._clear()
        self._write('help')
        ret = self._read_all()
        self._clear()
        return b"The following commands are available:" in ret

    @critical
    def switch_led_on(self):
        """ Switch on LED"""
        self._clear()
        self._write("led on")
        ret = self._read_all()
        return self._compare_output(ret, "Turning on the LED.")

    @critical
    def switch_led_off(self):
        """ Switch off LED"""
        self._clear()
        self._write("led off")
        ret = self._read_all()
        return self._compare_output(ret, "Turning off the LED.")

    @critical
    def blink_led(self, frequency=1):
        """ Blink LED 10 time with provided frequency
        @param frequency: Blinking frequency """
        self._clear()
        self._write(f"led blink {frequency}")
        ret = self._read_all()
        return self._compare_output(ret, f"Blinking the LED 10 times at {frequency} Hz.")

    @critical
    def get_temperature(self, address=None):
        """ Get temperature value for specific sensor
        @param address; I2C sensor address. If None use config to read back all temperatures """
        self._clear()
        self._write(f"temp {address}")
        ret = self._read_all()
        if ret == b'':
            return float('nan')
        else:
            return int(ret)

    @critical
    def get_all_temperatures(self):
        """ Get temperature values for all probes """
        temperatures = []
        for i, name in enumerate(REACHConfig()['ucontroller']['temperature']):
            self._clear()
            self._write(f"temp {i}")
            time.sleep(0.5)
            ret = self._read_all()
            try:
                temperatures.append((name, float(ret)))
            except:
                temperatures.append((name, float('nan')))

        return temperatures

    def close(self):
        """ Close serial port and itself, does not shutdown the microcontroller """
        self._conn.close()

    def reconnect(self):
        """ Reconnect to serial port"""
        if not self._conn.is_open:
            self._conn.open()

    @critical
    def _initialise_temperatures(self):
        """ Initialise temperature sensors """
        self._clear()
        self._write("temp init")
        ret = self._read_all()
        ret = ret.decode().strip().replace(r'\n', '.')

    @critical
    def _set_pwm(self, pin_number, duty_cycle_percentage=100):
        """ Generate pulse width modulation with a default frequency of 1000 Hz
        @param pin_number: pwm number, valid values 2 to 7
        @param duty_cycle_percentage: Duty cycle of wave in percentage """

        # Sanity checks
        if not 2 <= pin_number <= 7:
            logging.warning("Specified pwm identifier must be between 2 and 7. Ignoring")
            return

        if not 0 < duty_cycle_percentage <= 100:
            logging.warning("Specified pwm duty cycle percentage must be between 2 and 100. Ignoring")

        # Calculate duty cycle as 8 bit value
        duty_cycle = int(ceil((duty_cycle_percentage / 100) * 255))

        # Issue command
        self._read_all()
        self._write(f"pwm {pin_number} {duty_cycle}")
        ret = self._read_all()
        logging.info(f"Setting pwm {pin_number} to {duty_cycle}: {ret.decode().strip()}")

    @critical
    def get_gpio(self, number):
        """ Get currently set GPIO value
        @param number: GPIO number (1 - 53)"""

        # Sanity checks
        if not 1 <= number <= 53:
            logging.warning(f"Specified GPIO number {number} must be between 1 and 53. Cannot get. Ignoring")
            return

        # Issue command
        self._write(f"gpio {number}")
        try:
            return int(self._read_all().strip())
        except:
            return float('nan')

    @critical
    def set_gpio(self, number, value):
        """ Set GPIO pin to a specific number
        @param number: GPIO number (1 - 53)
        @param value: 1 or 0 """

        # Sanity checks
        if not 1 <= number <= 53:
            logging.warning(f"Specified GPIO number {number} must be between 1 and 53. Cannot set. Ignoring")
            return

        value = int(value)
        if value not in [0, 1]:
            logging.warning(f"Specified GPIO value {value} for number {number} must be 0 or 1. Cannot set. Ignoring")
            return

        # Issue command
        self._read_all()

        # Attempt to set the GPIO pin up to 3 times
        for _ in range(3):
            # Write the gpio value
            self._write(f"gpio {number} {value}")

            # Give it some time to stabilize
            time.sleep(0.01)

            # Check that the pin was set correctly. If not, try again
            if (ret := self.get_gpio(number, override_critical=True)) == value:
                # print(f"Set GPIO {number} to {value} and read back {ret}")
                return

            time.sleep(0.05)

        logging.error(f"Could not set gpio {number} to {value}")
        raise UControllerException(f"Could not set gpio {number} to {value}")

    def _toggle_gpio(self, number):
        """ Toggle the current value of the specified GPIO pin
        @param number: GPIO number (1 - 53)"""

        # Sanity checks
        if not 1 <= number <= 53:
            logging.warning(f"Specified GPIO number {number} must be between 1 and 53. Cannot toggle. Ignoring")
            return

        # Get current value
        current_value = self.get_gpio(number)

        # Toggle value
        value = "1" if self._compare_output(current_value, "0") else "0"

        # Write new value
        self.set_gpio(number, value)

    def _get_multiple_gpios(self, numbers: List[int]):
        """ Get the value of multiple GPIO pins
        @param numbers: List of pins """
        return [self.get_gpio(n) for n in numbers]

    def _set_multiple_gpios(self, numbers: List[int], value):
        """ Set the same value to  multiple GPIO pins
        @param numbers: List of pins
        @param value: Value to set """
        return [self.set_gpio(n, value) for n in numbers]

    def _toggle_multiple_gpios(self, numbers: List[int]):
        """ Toggle the value of multiple GPIO pins
        @param numbers: List of pins """
        return [self._toggle_gpio(n) for n in numbers]

    @critical
    def _print_help(self, option=""):
        """ Print help from running firmware
        @param option: Print detailed help for provided command  """
        self._clear()
        self._write(f'help {option}')
        print(self._read_all().decode())

    def _write(self, cmd):
        self._conn.write((cmd + self.term).encode())
        sleep(0.01)

    def _read_all(self):
        return self._conn.read_all()

    def _readline(self):
        return self._conn.readline()

    def _clear(self):
        self._read_all()

    def _load_power_configuration(self):
        """ Load power configuration """
        # Load power configuration and add state
        loaded = REACHConfig()['ucontroller_power']
        power = {}
        for k, v in loaded.items():
            state = 'on' if self.get_gpio(v) else 'off'
            power[k] = {'pin': v, 'state': state}
        return power

    def _load_switches_configuration(self):
        """ Load switch pin configuration"""
        loaded = REACHConfig()['ucontroller_switches']
        switches = {}
        for name, values in loaded.items():
            switches[name] = {'name': name, 'power': values['power']}
            pin_config = []
            for pin in values['pins']:
                state = 'on' if self.get_gpio(pin) else 'off'
                pin_config.append({'pin': pin, 'state': state})
            switches[name]['pins'] = pin_config

        return switches

    def _load_peripherals_configuration(self):
        """ Load peripheral pin configuration"""
        loaded = REACHConfig()['ucontroller_peripherals']
        peripherals = {}
        for k, v in loaded.items():
            state = 'on' if self.get_gpio(v) else 'off'
            peripherals[k] = {'pin': v, 'state': state}
        return peripherals

    @staticmethod
    def _load_sources_configuration():
        """ Load sources to switches configuration """
        loaded = REACHConfig()['ucontroller_sources']
        return loaded

    @staticmethod
    def _compare_output(output, string=""):
        return string.encode() == output.strip()


def interactive(instance):
    """ Interactive session with ucontroller """
    commands = ["switch_to_source", "set_gpio", "get_gpio", "standby", "raw_command", "quit", "help"]
    description = ["Switch to a specified source. Format: switch_to_source source_name",
                   "Set the specified value to a GPIO pin. Format: set_gpio pin_number value",
                   "Get the value of a GPIO pin. Format get_gpio pin_number",
                   "Place on standby",
                   "Send a raw command to the ucontroller. Format: raw_command command_to_send arguments",
                   "Exit interactive session",
                   "Display help text"]

    # Repeat until user exits
    while True:
        user_input = input("# ")
        if "switch_to_source" in user_input:
            command = [x for x in user_input.split(' ') if x != '']
            if len(command) != 2:
                print(f"Error parsing command {user_input}")
            else:
                try:
                    ucontroller.switch_to_source(command[1])
                except Exception as e:
                    pass

        elif "set_gpio" in user_input:
            command = [x for x in user_input.split(' ') if x != '']
            if len(command) != 3:
                print(f"Error parsing command {user_input}")
            else:
                try:
                    ucontroller.set_gpio(int(command[1]), command[2])
                except Exception as e:
                    print(f"Could not set GPIO: {e}")

        elif "get_gpio" in user_input:
            command = [x for x in user_input.split(' ') if x != '']
            if len(command) != 2:
                print(f"Error parsing command {user_input}")
            else:
                try:
                    print("Value is: ", ucontroller.get_gpio(int(command[1])))
                except Exception as e:
                    print(f"Could not set GPIO: {e}")

        elif "standby" in user_input:
            ucontroller.place_on_standby()

        elif "raw_command" in user_input:
            command = [x for x in user_input.split(' ') if x != '']
            if len(command) <= 1:
                print(f"Error parsing command {user_input}")
            else:
                try:
                   ucontroller._write(' '.join(command[1:]))
                   print(ucontroller._read_all().decode().strip())
                except Exception as e:
                    print(f"Could not set GPIO: {e}")

        elif "quit" in user_input or "exit" in user_input:
            print("Quitting interactive session")
            return

        elif "help" in user_input:
            for i, command in enumerate(commands):
                print(f"{command}: {description[i]}")

        else:
            print(f"Invalid input {user_input}")


if __name__ == "__main__":
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("--config", dest="config_file", default="reach",
                      help="Configuration file (default: reach)")
    parser.add_option("--initialise", dest="initialise", default=False, action="store_true",
                      help="Initialise the ucontroller (default: False")
    parser.add_option("--interactive", dest="interactive", default=False, action="store_true",
                      help="Start interactive session with ucontroller (default: False")
    (options, args) = parser.parse_args()

    # Sanity check on provided config file
    if not options.config_file.endswith(".yaml"):
        options.config_file += ".yaml"

    # Check that REACH_CONFIG_DIRECTORY is defined in the environment
    if "REACH_CONFIG_DIRECTORY" not in os.environ:
        print("REACH_CONFIG_DIRECTORY must be defined in the environment")
        exit()

    # Check if file exists
    full_path = os.path.join(os.environ['REACH_CONFIG_DIRECTORY'], options.config_file)
    if not os.path.exists(full_path) or not os.path.isfile(full_path):
        print(f"Provided file ({options.config_file}, fullpath {full_path}) could not be found or is not a file")
        exit()

    # Load configuration
    c = REACHConfig(options.config_file)

    # Connect to ucontroller and initialise if required
    ucontroller = Microcontroller(initialise=options.initialise)

    # If in interactive mode, start interaction
    if options.interactive:
        interactive(ucontroller)
