from datetime import datetime
import numpy as np
import logging
import ctypes
import time

from picosdk.usbtc08 import usbtc08 as tc08
from picosdk.functions import assert_pico2000_ok


class TC08:
    """ Class for interfacing with TC08 temperature probe.
    Needs libusbtc08-1 to be installed """
    def __init__(self, channel_names, simulate=False):

        # Set sensor channel names
        self._channel_names = channel_names

        # Set simulation flags
        self._simulate = simulate

        # Initialise sensor placeholders
        self._temperatures = (ctypes.c_float * 9)()
        self._overflow = ctypes.c_int16(0)
        self._units = tc08.USBTC08_UNITS["USBTC08_UNITS_CENTIGRADE"]

        # Initialise sensor
        if not self._simulate:
            self._sensor = self.initialise_sensor()

    def set_channel_name(self, channel, name):
        """ Set a specific channel's name """
        if channel >= len(self._channel_names):
            logging.error("Cannot assigned channel name to invalid channel")

        self._channel_names[channel] = name

    @staticmethod
    def initialise_sensor():
        """ Initialse temperature sensor """
        # Create chandle and status ready for use
        status = {}

        ret = tc08.usb_tc08_open_unit()
        assert_pico2000_ok(ret)
        chandle = ret

        try:
            # Set mains rejection to 50 Hz
            status["set_mains"] = tc08.usb_tc08_set_mains(chandle, 0)
            assert_pico2000_ok(status["set_mains"])

            # Set up channels (all type K, cold junction needs to be C)
            type_c = ctypes.c_int8(67)
            assert_pico2000_ok(tc08.usb_tc08_set_channel(chandle, 0, type_c))

            type_k = ctypes.c_int8(75)
            for channel in range(1, 9):
                assert_pico2000_ok(tc08.usb_tc08_set_channel(chandle, channel, type_k))
        except:
            logging.error("Error while setting up TC08. Not getting temperatures")
            tc08.usb_tc08_close_unit(chandle)
            return

        return chandle

    def read_temperatures(self):
        """ Read temperatures """

        # Get current timestamp
        timestamp = datetime.utcnow().timestamp()

        # If simulating, return fixed values
        if self._simulate:
            return timestamp, np.arange(len(self._temperatures))

        try:
            # Get temperatures
            assert_pico2000_ok(tc08.usb_tc08_get_single(self._sensor,
                                                        ctypes.byref(self._temperatures),
                                                        ctypes.byref(self._overflow),
                                                        self._units))
            # Get timestamp
            timestamp = time.time()

            return np.array(self._temperatures), timestamp
        except Exception as e:
            logging.error(f"Error while getting temperatures: {e}")
            self.close()
            return None, None

    def close(self):
        """ Close connection"""
        tc08.usb_tc08_close_unit(self._sensor)


if __name__ == "__main__":
    from pyreach.reach_config import REACHConfig

    conf = REACHConfig()['tc08_temperature_sensor']
    sensor = TC08(conf['channel_names'], simulate=False)
    print(sensor.read_temperatures())
    sensor.close()
    print("Closed")
