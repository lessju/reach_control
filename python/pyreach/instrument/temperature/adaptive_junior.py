# Information https://www.adaptivete.com/wp-content/uploads/Datasheets-TE_Controllers/
# ADJ-48-450-UR-JUNIOR-User-Guide-v104-compacted.pdf

import logging
import serial
import time

from pyreach.core.errors import ReachException
from pyreach.reach_config import REACHConfig
from pyreach.utils import connect_to_serial_device


class AdaptiveJunior:
    def __init__(self, timeout=1, simualte=False):
        """ Class initialiser """
        self._simulate = simualte
        self._timeout = timeout
        self._terminator = '\r\n'

        # Connect to module if not simulating
        self._connection = None
        if not self._simulate:
            self._connection = self.connect()

    def connect(self):
        config = REACHConfig()['adaptive_junior']
        connection = connect_to_serial_device(config['serial_id'], config['baudrate'])
        connection.timeout = self._timeout
        return connection

    def close(self):
        self._connection.close()

    def read_temperature(self):

        # If connection is None (simulating or could not detect device), return 0
        if self._connection is None:
            return 0

        # Clear any pending data
        if self._connection.inWaiting() > 0:
            print(self._connection.read(self._connection.inWaiting()))

        # Otherwise, issue request command
        self._connection.write(f'$REG 68{self._terminator}'.encode())

        # Wait for return
        time.sleep(0.2)

        # If a reply is available, parse it
        if self._connection.inWaiting() > 0:
            data = self._connection.read(self._connection.inWaiting())

            # Return temperature
            return float(data[7:-2])
        else:
            logging.warning("Adaptive Junior did not respond to read request!")
            return 0


if __name__ == "__main__":
    t = AdaptiveJunior()
    while True:
        print(t.read_temperature())
