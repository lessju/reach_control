#!/usr/bin/env python

import logging
import time
import enum
import math

from pyreach.reach_config import REACHConfig
from pyreach.utils import connect_to_serial_device


class TecTCMFault(enum.Enum):
    NONE = 0x00
    ADC_FAULT = 0x01
    ADCR_FAULT = 0x02
    VDC_LIMIT = 0x04
    TEMP_LIMIT = 0x08
    INHIBITED = 0x10


# Link: https://www.electrondynamics.co.uk/wp/product/peltier-tec-temperature-controller-tc-m-unit-2/
class TecTCM:
    def __init__(self, simulate=False):

        self._simulate = simulate

        # Connect to module if not simulating
        self._connection = None
        if not self._simulate:
            self._connection = self.connect()

        # Fault placeholder
        self._fault = TecTCMFault.NONE

    @property
    def fault(self):
        return self._fault

    def connect(self):
        config = REACHConfig()['tec_tcm_temperature_sensor']
        connection = connect_to_serial_device(config['serial_id'], config['baudrate'])
        connection.timeout = 0
        return connection

    def close(self):
        if self._connection is not None:
            self._connection.close()

    def configure(self):
        # If connection is None (simulating or could not detect device), return 0
        if self._connection is None:
            return 0

        # Clear any pending data
        self._clear_pending_data()

        # Generate command
        c = REACHConfig()['tec_tcm_temperature_sensor']
        command = f'\x01a204;{c["prop_term"]};{c["int_term"]};{c["deriv_term"]};1;0;1;'
        checksum = self._compute_checksum(command)

        # Issue request command and wait for reply
        self._connection.write(f'{command}{checksum}'.encode('ascii'))
        self._wait_for_reply()

        # Make sure command was performed correctly
        self._connection.write('\x01b00C3'.encode('ascii'))
        self._wait_for_reply()

        # If a reply is available, parse it
        if self._connection.inWaiting() > 0:
            data = self._connection.read(self._connection.inWaiting())
            data = data.decode().split(';')

            if not (math.isclose(float(data[1]), c["prop_term"]) and math.isclose(float(data[2]), c["int_term"]) and
                    math.isclose(float(data[3]), c["deriv_term"])):
                logging.error("Could not set TEC TCM configuration")
            else:
                logging.info("TEC TCM configured successfully")
        else:
            logging.warning("TecTM did not respond to command request!")

    def set_target_temperature(self, temperature):
        """ Set target temperature """
        # If connection is None (simulating or could not detect device), return 0
        if self._connection is None:
            return 0

        self._clear_pending_data()

        # Generate command
        command = f'\x01i111;{temperature};100;0;'
        checksum = self._compute_checksum(command)

        # Issue `request command and wait for reply
        self._connection.write(f'{command}{checksum}'.encode('ascii'))
        self._wait_for_reply()

        logging.info("Temperature set")

    def set_output(self, power_on=True):
        """ Turn the output drive on or off """
        # If connection is None (simulating or could not detect device), return 0
        if self._connection is None:
            return 0

        self._clear_pending_data()

        # Generate command
        if power_on:
            command = '\x01m041;0;'
        else:
            command = '\x01m040;0;'
        checksum = self._compute_checksum(command)

        # Issue request command and wait for reply
        self._connection.write(f'{command}{checksum}'.encode('ascii'))
        self._wait_for_reply()

        if power_on:
            logging.info("Output drive enabled")
        else:
            logging.info("Output drive disabled")

    def read_temperature(self):

        # If connection is None (simulating or could not detect device), return 0
        if self._connection is None:
            return 0

        self._clear_pending_data()

        # Otherwise, issue request command and wait for a reply
        self._connection.write('\x01j00CB'.encode('ascii'))
        self._wait_for_reply()

        # If a reply is available, parse it
        if self._connection.inWaiting() > 0:
            data = self._connection.read(self._connection.inWaiting())
            data = data.decode().split(';')

            # Check if there are any faults
            try:
                self._fault = TecTCMFault(int(data[5]))
            except:
                pass

            # Return temperature
            try:
                return float(data[1])
            except:
                print("Failed")
                return 0
        else:
            logging.warning("TecTM did not respond to read request!")
            return 0

    def _clear_pending_data(self):
        # Clear any pending data
        if self._connection.inWaiting() > 0:
            self._connection.read(self._connection.inWaiting())

    def _wait_for_reply(self, expected_read_size=40, timeout=0.5):
        # Wait for return or until more than 0.5 seconds have elapsed
        started = time.time()
        while self._connection.inWaiting() < expected_read_size and time.time() - started < timeout:
            time.sleep(0.05)

    @staticmethod
    def _compute_checksum(command):
        checksum = hex(sum([ord(c) for c in command]) & 0xFF)
        checksum = str(checksum)[2:].upper()
        if len(checksum) == 1:
            return '0' + checksum
        else:
            return checksum


if __name__ == "__main__":
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("--configure", dest="configure", default=False, action="store_true",
                      help="Configure the TEC module (default: False")
    parser.add_option("--set-target-temperature", dest="temperature", default=None, action="store", type=int,
                      help="Set target temperature (default: do not set")
    parser.add_option("--enable-output", dest="enable_output", default=False, action="store_true",
                      help="Enable temperature control (default: False")
    parser.add_option("--disable-output", dest="disable_output", default=False, action="store_true",
                      help="Disable temperature control (default: False)")
    (options, args) = parser.parse_args()

    tcm = TecTCM()

    if options.temperature is not None:
        tcm.set_target_temperature(options.temperature)

    if options.enable_output:
        tcm.set_output(True)
    elif options.disable_output:
        tcm.set_output(False)

    if options.configure:
        tcm.configure()

    if not any([options.enable_output, options.disable_output, options.configure]):
        while True:
            print(f"{tcm.read_temperature():.3f}C")
            time.sleep(3)
