from enum import Enum


# Define an enumeration of controllable devices in the REACH instrument
class Device(Enum):
    TPM = 1
    UCONTROLLER = 2
    VNA = 4
    PDU = 8
    POWER_SENSOR = 16
    TC08 = 32
    ADAPTIVE_JUNIOR = 64
    TEC_TCM = 128


if __name__ == "__main__":
    print(Device.TPM)
