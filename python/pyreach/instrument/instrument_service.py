#!/usr/bin/env python

import signal
import logging
import threading
import time

from pyreach.instrument.devices import Device
from pyreach.digital_backend.spectrometer import Spectrometer
from pyreach.instrument.power.power_meter import PowerMeter
from pyreach.instrument.temperature.adaptive_junior import AdaptiveJunior
from pyreach.instrument.temperature.tc08 import TC08
from pyreach.instrument.temperature.tec_tcm import TecTCM
from pyreach.instrument.ucontroller.microcontroller import Microcontroller
from pyreach.repository.models import Power, Temperature, TPMStatus, get_probe_foreign_keys
from pyreach.reach_config import REACHConfig


class REACHInstrument:
    """ This class represents the (hardware) instrument as a whole, contains logic
    to set up the entire system and for continuous monitoring. It should be treated as
    a service, or will be called by a running service """

    def __init__(self):
        """ Class initializer """
        # Initialise all devices which need to be monitoring

        # Placeholders for all devices
        self._spectrometer = None
        self._ucontroller = None
        self._digital_box_temperature = None
        self._digital_box_power = None
        self._tec_tcm = None
        self._tc08 = None

        # Load settings
        config = REACHConfig()['software_framework']
        self._sleep_duration = config.get('instrument_monitor_cadence', 60)
        self._device_recheck = config.get('instrument_monitor_hw_recheck', 300)

        # List containing pointer to monitoring threads
        self._monitoring_threads = []

        # Flag to stop all threads
        self._stop = False

        # Initialise system devices
        self.initialise()

        # Local store of foreign keys (for speed)
        self._device_foreign_keys = get_probe_foreign_keys()

    def initialise(self):
        """ Initialise all the devices """

        self._connect_to_backend()
        # self._connect_to_power_meter()
        self._connect_to_tc08()
        self._connect_to_tec_tcm()
        self._connect_to_adaptive_junior()

        # Not using ucontroller at the moment, however, it's use has to be shared with other processes,
        # so the connection must be open and closed in a system-wide critical section
        # self._connect_to_ucontroller()

    def start_service(self):
        """ Start monitoring and control threads """

        # Start instrument monitor
        t = threading.Thread(target=self._monitor_instrument)
        t.name = "Instrument Monitor"
        t.start()
        self._monitoring_threads.append(t)

    def stop_service(self):
        """ Stop all monitoring and control threads """

        # Set stopping flag
        self._stop = True

        # Wait for all threads to finish
        for t in self._monitoring_threads:
            t.join()

        # Clear list of monitoring threads
        self._monitoring_threads = []

    def _monitor_instrument(self):
        """ Get instrument metrics and store in database """

        # Determine last recheck and logging time
        last_recheck = time.time()
        last_log = time.time()

        # Loop until terminated
        while not self._stop:

            # Determine whether it's time to recheck or not
            recheck = False
            if time.time() - last_recheck > self._device_recheck:
                recheck = True
                last_recheck = time.time()

            # Get start time of current iteration
            t_start = time.time()

            # Get info from TPM
            if self._spectrometer is not None:
                try:
                    TPMStatus(receiver_id="Antenna 0",
                              board_temperature=self._spectrometer.get_board_temperature(),
                              fpga1_temperature=self._spectrometer.get_fpga1_temperature(),
                              fpga2_temperature=self._spectrometer.get_fpga2_temperature(),
                              voltage=self._spectrometer.get_voltage()).save()
                except Exception as e:
                    logging.error(f"Could not get metrics from TPM: {e}")
                    self._spectrometer = None
            elif recheck:
                self._connect_to_backend()

            # This service always disconnects from the ucontroller after reading the temperature values
            # to avoid conflicts with other processes using it
            try:
                # Connect to ucontroller
                if not self._ucontroller:
                    self._connect_to_ucontroller()
                else:
                    self._ucontroller.reconnect()

                # Get temperatures
                temps = self._ucontroller.get_all_temperatures()
                for i, temps in enumerate(temps):
                    Temperature(probe=self._device_foreign_keys[(Device.UCONTROLLER, i)],
                                temperature=temps[1]).save()
            except Exception as e:
                logging.error(f"Could not get metrics from uController: {e}")
            finally:
                if self._ucontroller:
                    self._ucontroller.close()

            # Get info from power meter:
            # if self._digital_box_power is not None:
            #     try:
            #         Power(probe=self._device_foreign_keys[(Device.POWER_SENSOR, 0)],
            #               power=self._digital_box_power.measure_power()).save()
            #     except Exception as e:
            #         logging.error(f"Could not get metrics from Power meter: {e}")
            #         self._digital_box_power.close()
            #         self._digital_box_power = None
            # elif recheck:
            #     self._connect_to_power_meter()

            # Get info from adaptive junior
            if self._digital_box_temperature is not None:
                try:
                    Temperature(probe=self._device_foreign_keys[(Device.ADAPTIVE_JUNIOR, 0)],
                                temperature=0).save()
                except Exception as e:
                    logging.error(f"Could not get metrics from adaptive junior: {e}")
                    self._digital_box_temperature.close()
                    self._digital_box_temperature = None
            elif recheck:
                self._connect_to_adaptive_junior()

            # Get info from Tec Tcm
            if self._tec_tcm is not None:
                try:
                    Temperature(probe=self._device_foreign_keys[(Device.TEC_TCM, 0)],
                                temperature=self._tec_tcm.read_temperature()).save()
                except Exception as e:
                    logging.error(f"Could not get metrics from Tec Tcm: {e}")
                    self._tec_tcm.close()
                    self._tec_tcm = None
            elif recheck:
                self._connect_to_tec_tcm()

            # Get info from TC08
            if self._tc08 is not None:
                try:
                    temperatures, _ = self._tc08.read_temperatures()
                    for i, t in enumerate(temperatures):
                        Temperature(probe=self._device_foreign_keys[(Device.TC08, i)],
                                    temperature=t).save()
                except Exception as e:
                    logging.error(f"Could not get metrics from TC08: {e}")
                    self._tc08.close()
                    self._tc08 = None
            elif recheck:
                self._connect_to_tc08()

            # Only updates every 30 minutes
            if time.time() - last_log > 1800:
                logging.info("Updated metrics")
                last_log = time.time()

            # Sleep for a while
            self.wait_for(self._sleep_duration - (time.time() - t_start))

    def _disconnect_from_devices(self):
        """ Disconnect from devices requiring a disconnect """
        if self._ucontroller:
            self._ucontroller.close()

        if self._tc08:
            self._tc08.close()

        if self._tec_tcm:
            self._tec_tcm.close()

        if self._digital_box_temperature:
            self._digital_box_temperature.close()

        if self._digital_box_power:
            self._digital_box_power.close()

    def _connect_to_ucontroller(self):
        """ Connect to the ucontroller """

        try:
            self._ucontroller = Microcontroller(initialise=False)
            logging.info("Connected to ucontroller")
        except Exception as e:
            logging.error("Could not connect to ucontroller, check configuration and hardware: {}".format(e))
            self._ucontroller = None

    def _connect_to_backend(self):
        """ Connect to the digital backend and check whether it is programmed and initialised.
            If not, set it up"""

        # Load spectrometer configuration
        conf = REACHConfig()['spectrometer']

        try:
            self._spectrometer = Spectrometer(ip=conf['ip'], port=conf['port'],
                                              lmc_ip=conf['lmc_ip'], lmc_port=conf['lmc_port'],
                                              enable_spectra=False)
            self._spectrometer.connect()
        except Exception as e:
            logging.error("Could not connect to digital backend, check configuration and hardware: {}".format(e))
            self._spectrometer = None
            return

        # Check if the digital backend is configured properly
        if not self._spectrometer.is_programmed():
            logging.error("Could not configure digital backend, check configuration and hardware")
            self._spectrometer = None
        else:
            logging.info("Connected to spectrometer")

    def _connect_to_tc08(self):
        """ Connect to TC08 temperature sensor """

        conf = REACHConfig()['tc08_temperature_sensor']

        try:
            self._tc08 = TC08(conf['channel_names'], simulate=False)
            logging.info("Connected to TC08")
        except Exception as e:
            logging.error("Could not connect to TC08, check configuration and hardware: {}".format(e))
            self._tc08 = None
            return

    def _connect_to_tec_tcm(self):

        try:
            self._tec_tcm = TecTCM()
            logging.info("Connected to Tec Tcm")
        except Exception as e:
            logging.error("Could not connect to Tec Tcm, check configuration and hardware: {}".format(e))
            self._tec_tcm = None
            return

    def _connect_to_adaptive_junior(self):
        """ Connect to TPM box temperature sensor """

        try:
            self._digital_box_temperature = AdaptiveJunior()
            logging.info("Connected to adaptive junior probe.")
        except Exception as e:
            logging.error("Could not connect to Adaptive Junior, check configuration and hardware: {}".format(e))
            self._digital_box_temperature = None
            return

    def _connect_to_power_meter(self):
        """ Connect to power meter in TPM box """
        conf = REACHConfig()['power_meter']

        try:
            self._digital_box_power = PowerMeter(float(conf['read_frequency']))
            logging.info("Connected to power meter")
        except Exception as e:
            logging.error("Could not connect to power meter, check configuration and hardware: {}".format(e))
            self._digital_box_power = None

    def wait_for(self, duration):
        """ Wait for a given duration """
        start_time = time.time()
        while time.time() - start_time < duration and not self._stop:
            time.sleep(1)

    def is_service_running(self):
        return len(self._monitoring_threads) > 0

    def register_signal_handler(self):
        def signal_handler(signum, frame):
            # Stop observer and data acquisition
            logging.info("Received interrupt, stopping monitoring")
            self.stop_service()
            time.sleep(1)

        signal.signal(signal.SIGINT, signal_handler)


if __name__ == "__main__":
    # Load default reach configuration
    REACHConfig()

    # Instantiate instrument and initialise
    instrument = REACHInstrument()

    # Start monitoring
    instrument.start_service()
    instrument.register_signal_handler()

    # Wait for service to stop
    while instrument.is_service_running():
        time.sleep(1)
