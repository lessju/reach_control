import datetime
import glob
import os
import re

import h5py
import numpy as np
import plotly.graph_objects as go
from flask import Flask, request
from slack_bolt import App, Say
from slack_bolt.adapter.flask import SlackRequestHandler

from pyreach.core.errors import ObservationInThePastException, ObservationsConflictException, \
    ObservationDoesNotExistException
from pyreach.core.slack import ReachSlack
from pyreach.instrument.devices import Device
from pyreach.instrument.ucontroller.microcontroller import Microcontroller
from pyreach.reach_config import REACHConfig
from pyreach.repository.metric_data import MetricData
from pyreach.repository.models import get_pending_observations, get_running_observation
from pyreach.service.scheduler import Scheduler

# Initialise Flask app
from utilities.ucontroller_pins_status import get_pin_status

app = Flask(__name__)

# Initialise Slack notifications
slack_notifications = ReachSlack(REACHConfig()['software_framework']['slack_token_file'],
                                 secret_file=REACHConfig()['software_framework']['slack_secret_file'])

# Generate BOLT app and Slack request handler
bolt_app = App(token=slack_notifications.slack_token, signing_secret=slack_notifications.slack_secret)
handler = SlackRequestHandler(bolt_app)

# Load default configuration
reach_config = REACHConfig()

# Instantiate scheduler and metric data
scheduler = Scheduler()
metrics = MetricData()


@app.route("/reach_instrument/events", methods=["POST"])
def slack_events():
    """ Declaring the route where slack will post a request """
    return handler.handle(request)


@bolt_app.event("app_mention")
def mention(payload: dict, say: Say):
    # TODO: Check whether message came from the reach-control channel
    # TODO: Check whether message came from list of approved users (if required)

    request = payload.get("text").lower()

    # Check for greeting (useful for debugging connection)
    if 'hello' in request:
        say(f"Hi <@{payload.get('user')}>")

    # Check if a calibration run was requested
    elif 'schedule calibration' in request:

        # Extract parameters
        params = parse_observation_parameters(request)

        # Schedule new observation
        try:
            scheduler.schedule_new_observation(config_path='reachcal1.yaml',
                                               start_time=params['start_time'],
                                               calibration=True)
        except ObservationsConflictException:
            say("Failed to schedule calibration observation as it conflicts with an existing one")
        except ObservationInThePastException:
            say("Failed to schedule calibration observation as it start in the past")
        except Exception as e:
            say(f"Failed to schedule calibration observation: {e}")
        else:
            say("Calibration observation scheduled successfully")

    # Check if an observation was requested
    elif 'schedule observation' in request:
        # Extract parameters
        params = parse_observation_parameters(request)

        # Schedule new observation
        try:
            scheduler.schedule_new_observation(config_path='reach.yaml',
                                               start_time=params['start_time'],
                                               calibration=False)
        except ObservationsConflictException:
            say("Failed to schedule sky observation as it conflicts with an existing one")
        except ObservationInThePastException:
            say("Failed to schedule sky observation as it starts in the past")
        except Exception as e:
            say(f"Failed to schedule sky observation: {e}")
        else:
            say("Sky observation scheduled successfully")

    # Check whether the observation schedule was requested
    elif 'get schedule' in request:

        running = get_running_observation()
        if len(running) != 0:
            say(f"{running[0].observation_type.name} observation {running[0].observation_name} running since {running[0].start_time} and should "
                f"finish at {running[0].stop_time}")

        pending = get_pending_observations()
        for obs in get_pending_observations():
            say(f"{obs.observation_type.name} observation {obs.observation_name} will start at {obs.start_time} and "
                f"should finish at {obs.stop_time} [ID: {obs.id}]")

        if len(running) + len(pending) == 0:
            say("No running of pending observations")

    # Check whether a request to delete a scheduled observation was received
    elif "delete observation" in request:
        if (obs_id := re.match(r".*id (?P<id>\w+).*", request)) is not None:
            obs_id = obs_id.groupdict()['id']

        try:
            scheduler.delete_observation(obs_id)
        except ObservationDoesNotExistException:
            say(f"Could not find observation with id {obs_id}")
        except Exception as e:
            say(f"Failed to delete observation with id {obs_id}: {e}")
        else:
            say(f"Successfully deleted observation with id {obs_id}")

    elif 'tpm status' in request:
        say(get_tpm_status())

    elif 'ucontroller status' in request:
        say(f"```{get_pin_status()}```")

    elif 'standby' in request:
        ucontroller = Microcontroller(initialise=False)
        try:
            ucontroller.place_on_standby()
            say("Placed receiver on standby")
        except Exception as e:
            say(f"Failed to place receiver on standby: {e}")

    elif 'get temperatures' in request:
        say(f"```{get_latest_temperatures()}```")

    elif 'plot antenna bandpass' in request:
        image = get_latest_spectrum()
        if image is None:
            say("Could not get antenna spectrum")
        else:
            say.client.files_upload_v2(channel=payload.get('channel'),
                                       file=image,
                                       title="Latest antenna spectrum")

    elif 'plot' in request:
        html, image = generate_temperature_plot()

        if 'html' in request:
            say.client.files_upload_v2(channel=payload.get('channel'),
                                       file=html,
                                       title="Latest temperatures plot")
        else:
            say.client.files_upload_v2(channel=payload.get('channel'),
                                       file=image,
                                       title="Latest temperatures plot")
    else:
        say("Unknown request")


def get_tpm_status():
    """ Get the latest TPM status and generate reply string """
    data = metrics.get_latest_tpm_status()

    if data is None:
        return "Could not get latest TPM status"

    reply = f"TPM Status as of {data['Datetime']}\n"
    for k, v in data.items():
        if k == "Datetime":
            continue
        reply += f"{k}: {v}\n"
    return reply


def get_latest_temperatures():
    """ Get the latest measured temperatures and generate reply string"""

    data = metrics.get_latest_temperatures()

    if data is None:
        return "Could not get latest temperatures"

    reply = ""
    for k, v in sorted(data.items()):
        reply += f"{v[0]}:\t{k}\t\t({v[1]})\n"
    return reply


def get_latest_spectrum():
    """ Get the latest saved spectrum """

    # Go through data directory and sub-directories in descending order
    output_dir = reach_config['observation']['output_directory']
    for directory in sorted(os.listdir(output_dir), reverse=True):
        path = os.path.join(output_dir, directory)
        if not os.path.isdir(path):
            continue

        for time in sorted(os.listdir(os.path.join(output_dir, directory)), reverse=True):
            path = os.path.join(path, time)
            if not os.path.isdir(path):
                continue

            # Check if a hdf5 file is preset
            for file in glob.glob(os.path.join(path, '*.hdf5')):
                filepath = os.path.join(path, file)

                # If an antenna spectrum is present, load it
                data, timestamp = None, None
                with h5py.File(filepath) as f:
                    if 'observation_data' in f.keys() and 'ant_spectra' in f['observation_data'].keys():
                        data = f['observation_data']['ant_spectra'][-1, :]
                        timestamp = f['observation_data']['ant_timestamps'][-1, 0]
                        timestamp = datetime.datetime.utcfromtimestamp(timestamp)

                # If antenna spectrum is present, generate a plot, save it as an image and return the image path
                if data is not None:
                    fig = go.Figure()
                    x = np.linspace(0, 200, reach_config['spectrometer']['nof_frequency_channels'])[2:]
                    y = data[2:]
                    y = 10 * np.log10(y)
                    fig.add_trace(go.Scatter(x=x, y=y, mode="lines"))
                    fig.update_layout(title=f"Latest Antenna Spectrum - {timestamp.strftime('%d-%m-%y %H:%M:%S')}",
                                      yaxis_title='Power (arbitrary)',
                                      xaxis_title='Frequency (MHz)', width=800, height=600,
                                      margin=dict(l=25, r=25, b=50, t=50, pad=4)
                                      )

                    fig.write_image("/tmp/spectrum.png")
                    return "/tmp/spectrum.png"

    return None


def generate_temperature_plot():
    """ Generate a temperature plot for the previous 24 hours"""
    ret = metrics.get_receiver_temperatures(start_date=datetime.datetime.utcnow() - datetime.timedelta(hours=24))

    id_map = {(Device.TEC_TCM, 0): "TEC TCM"}
    for i, probe in enumerate(REACHConfig()['tc08_temperature_sensor']['channel_names']):
        id_map[(Device.TC08, i)] = f'TC08 - {list(probe.keys())[0]}'

    for i, probe in enumerate(REACHConfig()['ucontroller']['temperature']):
        id_map[(Device.UCONTROLLER, i)] = f'Ucontroller - {probe}'

    fig = go.Figure()
    for probe in ret.keys():
        data = np.array(ret[probe])
        if data.size == 0:
            continue

        fig.add_trace(go.Scatter(x=data[:, 0], y=data[:, 1], mode="lines", name=id_map[probe]))

    fig.write_html("/tmp/test.html")
    fig.write_image("/tmp/test.png")
    return "/tmp/test.html", "/tmp/test.png"


def parse_observation_parameters(request_string):
    """ Parse an observation request to extract observation parameters """

    params = {}

    # Determine whether a start datetime was included
    start_time = re.match(r"(.*at)?.*.*(?P<start_time>\d{2}/\d{2}/\d{4}_\d{2}:\d{2}).*", request_string)
    if start_time is not None:
        params['start_time'] = start_time.groupdict()['start_time']
    else:
        params['start_time'] = datetime.datetime.utcnow().strftime("%d/%m/%Y_%H:%M")

    return params


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
