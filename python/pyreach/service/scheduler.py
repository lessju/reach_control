#!/usr/bin/env python

import ast
import copy
import datetime
import logging
import os
import threading
import time
from datetime import timedelta

import pytz
import yaml
from mongoengine import DoesNotExist

from pyreach.core.errors import ObservationsConflictException, ObservationInThePastException, \
    ObservationDoesNotExistException
from pyreach.reach_config import REACHConfig
from pyreach.observation import REACHObservation
from pyreach.repository.models import Observation, check_observation_overlap, get_pending_observations, \
    ObservationType, ObservationStatus


class Scheduler:

    def __init__(self,):
        """ Class initializer  """

        # Load configuration
        REACHConfig()

        # Initialise scheduling thread and stop event
        self._stop_event = threading.Event()
        self._scheduling_thread = threading.Thread(target=self._scheduling_loop)

    def start(self):
        """ Start the scheduling service  """
        self._scheduling_thread.start()

    def stop(self):
        """ Stop the scheduler"""
        self._stop_event.set()
        self._scheduling_thread.join()

    def _scheduling_loop(self):
        """ Run the scheduling loop """

        logging.info('REACH Scheduler service started')

        # Run until stopped
        while not self._stop_event.is_set():

            # Get current pending observations
            pending_observations = get_pending_observations()

            if pending_observations:
                next_observation = pending_observations[0]
                current_time = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
                time_difference = (next_observation.start_time.replace(tzinfo=pytz.utc) - current_time).total_seconds()

                # If the next pending observation is in the past, then something must have gone wrong. Change the
                # observation status to "not scheduled"
                if time_difference < 60:
                    logging.warning(f"Observation {next_observation.observation_name} scheduled at "
                                    f"{next_observation.start_time} as not run.")
                    next_observation.status = ObservationStatus.NOT_SCHEDULED
                    next_observation.save()

                elif abs(time_difference) < 60:
                    logging.info(f"Launching observation {next_observation.observation_name}")

                    # Create observation instance and run
                    c = yaml.safe_load(next_observation.configuration)
                    obs = REACHObservation(c['observation'],
                                           c['operations'],
                                           c['configuration'] if 'configuration' in c.keys() else None,
                                           override_signal_handler=False)

                    next_observation.observation_status = ObservationStatus.RUNNING
                    next_observation.sav()

                    try:
                        obs.run_observation()
                    except:
                        logging.error(f"An error occurred when running observation {next_observation.observation_name}")
                        next_observation.observation_status = ObservationStatus.FAILED
                    else:
                        next_observation.observation_status = ObservationStatus.FINISHED

            # Sleep for one second
            time.sleep(5)

        logging.info('REACH Scheduler service stopped')

    def is_service_running(self):
        return self._scheduling_thread.is_alive()

    def schedule_new_observation(self, config_path, start_time, name="reach_observation",
                                 config_override=None, calibration=True):
        """ Schedule a new observation """

        # Sanity checking on time
        start_time = datetime.datetime.strptime(start_time, "%d/%m/%Y_%H:%M").replace(tzinfo=pytz.utc)
        if start_time + timedelta(10) < datetime.datetime.utcnow().replace(tzinfo=pytz.utc):
            logging.error("Scheduled observation must start at least 10 seconds in the future")
            raise ObservationInThePastException()

        # Determine observation duration
        duration = self._estimate_observation_duration(config_path)
        end_time = start_time + timedelta(seconds=duration)

        # Check whether there is a conflict with existing observations
        if not check_observation_overlap(start_time, end_time):
            # No observation overlap, create new observation and save to database

            # Store configuration as string and update required fields
            with open(os.path.join(REACHConfig().get_root_path(), config_path)) as f:
                config = yaml.safe_load(f)
                config['observation']['name'] = name
                config['observation']['start_time'] = start_time.strftime("%d/%m/%Y_%H:%M")

                # Apply any configuration overrides
                config = self.override_configuration_items(config, config_override)

                Observation(observation_name=name,
                            observation_type=ObservationType.CALIBRATION if calibration else ObservationType.SKY,
                            start_time=start_time,
                            stop_time=end_time,
                            configuration=str(config)).save()

            logging.info(f"Observation {name} scheduled")
        else:
            logging.error(f"Could not schedule observation {name} as it conflicts with an existing one")
            raise ObservationsConflictException()

    @staticmethod
    def delete_observation(observation_id):
        """ Delete and existing observation """
        try:
            obs = Observation.objects.get(_id=observation_id)
            obs.delete()
            logging.info(f"Successfully deleted observation with id {observation_id}")
        except DoesNotExist:
            logging.error(f"Observation {observation_id} does not exist")
            raise ObservationDoesNotExistException

    @staticmethod
    def list_pending_observations():
        """ List currently pending observations """
        for obs in get_pending_observations():
            logging.info(f"Observation {obs.observation_name} ({obs._id}) will run from "
                         f"{obs.start_time.strftime('%d/%m/%Y_%H:%M')} " 
                         f"to {obs.stop_time.strftime('%d/%m/%Y_%H:%M')}")

    @staticmethod
    def _estimate_observation_duration(config_path):
        """ Estimate the duration of a provided observation configuration """

        # Load configuration defined in provided config file (only observation file)
        c = REACHConfig(config_file_path=config_path, load_database=True)
        c.load_from_file(config_path)

        # Perform an observation dry run to calculate the approximate duration of the observation
        # Logging is disabled whilst doing so
        logging.disable(logging.CRITICAL)
        obs = REACHObservation(c['observation'],
                               c['operations'],
                               c['configuration'] if 'configuration' in c.keys() else None,
                               override_signal_handler=False)
        duration = obs.dry_run_operations()
        logging.disable(logging.NOTSET)

        return duration

    @staticmethod
    def override_configuration_items(configuration, override_items):
        """ Override configuration items """
        if override_items is None:
            return configuration

        original_config = copy.copy(configuration)

        try:
            for item in override_items.split(';'):
                key, value = item.split('=')
                value = ast.literal_eval(value)

                current_level = configuration
                for subkey in key.split('.')[:-1]:
                    if subkey in current_level.keys():
                        current_level = current_level[subkey]
                    else:
                        raise KeyError(f"Invalid configuration item '{key}'")

                key_to_change = key.split('.')[-1]
                if key_to_change in current_level.keys():
                    current_level[key_to_change] = value
                else:
                    raise KeyError(f"Invalid configuration item '{key}'")
        except Exception as e:
            logging.warning(f"Could not override configuration: {e}. Using original")
            return original_config

        return configuration


if __name__ == "__main__":
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-S", "--schedule", dest="new_obs", default=False, action="store_true",
                      help="Schedule a new observation. Configuration file and start time required (default: False)")
    parser.add_option("-B", "--batch-schedule", dest="new_batch", default=False, action="store_true",
                      help="Batch schedule. Batch file required (default: False)")
    parser.add_option("-D", "--delete", dest="delete_obs", default=False, action="store_true",
                      help="Delete an existing observation, observation ID required (default: False)")
    parser.add_option("-L", dest="list_pending", default=False, action="store_true",
                      help="List pending observations (default: False)")
    parser.add_option("-R", dest="run", default=False, action="store_true",
                      help="Run scheduler (default: False)")

    parser.add_option("--obs-config", dest="obs_config", default=None,
                      help="Configuration file")
    parser.add_option("--obs-name", dest="obs_name", default="reach_observation",
                      help="Observation name (default: reach_observation")
    parser.add_option("--obs-start-time", dest="obs_start", default=None,
                      help="Observation start time (format: %d/%m/%Y_%H:%M")
    parser.add_option("--obs-config-override", dest="config_override", default=None,
                      help="Define a semicolon separated list of config to override for this observation. "
                           "Example usage: configuration.integration_time=10;observation.spectrometer_channel_id=1")
    parser.add_option("--obs-batch-file", dest="obs_batch", default=None,
                      help="CSV file containing list observations to schedule in bulk. Format of CSV file should be: "
                           "config, date_time, name (can be left empty), config_override (can be left empty). "
                           "Same formatting for individual items as per command line arguments.")

    parser.add_option("--obs-id", dest="obs_id", default=None,
                      help="Observation ID. Can be a comma-separated list for deleting multiple observations")

    (options, args) = parser.parse_args()

    # Check that an action has been specified
    if not any([options.new_obs, options.delete_obs, options.list_pending, options.new_batch, options.run]):
        print("No scheduler action specified, exiting")
        exit()

    # Load default configuration
    REACHConfig()

    # Parameters sanity check
    if options.new_obs and (options.obs_config is None or options.obs_start is None):
        logging.error("Configuration file and start time required to schedule a new observation")
        exit()
    elif options.delete_obs and options.obs_id is None:
        logging.error("Observation ID required to delete an observation")
        exit()
    elif options.new_batch:
        if options.obs_batch is None:
            logging.error("Batch file with observation entries required for batch scheduling")
            exit()
        elif not (os.path.exists(options.obs_batch) and os.path.isfile(options.obs_batch)):
            logging.error("Invalid batch file path specified")
            exit()

    # Create scheduler instance
    s = Scheduler()

    # Perform required action
    if options.list_pending:
        s.list_pending_observations()

    elif options.delete_obs:
        for obs_id in options.obs_id.split(','):
            s.delete_observation(obs_id)

    elif options.new_obs:
        s.schedule_new_observation(options.obs_config, options.obs_start,
                                   options.obs_name, options.config_override)

    elif options.new_batch:
        with open(options.obs_batch, 'r') as f:
            for line in f.readlines():
                try:
                    config, date_time, name, override = line.split(',')
                    s.schedule_new_observation(config, date_time, name, override)
                except Exception as e:
                    logging.error(f"Could not schedule observation with parameters {line}: {e}")

    elif options.run:
        s.start()
        while s.is_service_running():
            time.sleep(1)
