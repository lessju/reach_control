#!/usr/bin/env python
from pyreach.instrument.ucontroller.microcontroller import Microcontroller
from pyreach.reach_config import REACHConfig

if __name__ == "__main__":

    conf = REACHConfig()['ucontroller']
    ucontroller = Microcontroller(initialise=False)
    ucontroller.turn_off()
