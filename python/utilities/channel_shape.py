import time
import math
import socket
import h5py
import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser
from acquire_spectra import SpeadRx
from pyreach.spectrometer.spectrometer import Spectrometer

class SocketManager():
    def __init__(self, port=4660):
        self.port = port
        self.sock = 0

    def open(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)   # UDP
        self.sock.bind(("0.0.0.0", 4660))
        self.sock.settimeout(1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 2*1024*1024)
        return self.sock

    def close(self):
        self.sock.close()


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-p",
                      dest="port",
                      default="4660",
                      help="UDP port")
    # parser.add_option("-a",
    #                   dest="accu",
    #                   default="1",
    #                   help="Number of spectra to accumulate")
    parser.add_option("-w",
                      dest="write_hdf5",
                      default=False,
                      action="store_true",
                      help="Write HDF5 files")
    parser.add_option("-c",
                      dest="channel_id",
                      default="8192",
                      help="Number of spectra to accumulate")
    parser.add_option("-s",
                      dest="span",
                      default=1.5,
                      type = "float",
                      help="Span in channel width unit from channel_id center frequency")
    parser.add_option("-n",
                      dest="nof_point",
                      default="16",
                      help="Number of evaluated frequency points")
    # parser.add_option("-i",
    #                   dest="ignore_fpga2",
    #                   default=False,
    #                   action="store_true",
    #                   help="Ignore data from FPGA2")

    (options, args) = parser.parse_args()

    nof_fpga = 1

    rx_socket = SocketManager(4660)
    spead_rx_inst = SpeadRx(options.write_hdf5, nof_fpga, False)

    spectrometer = Spectrometer("10.0.10.3", #ip=conf['ip'],
                                10000,#port=conf['port'],
                                "10.0.10.1", #lmc_ip=conf['lmc_ip'],
                                4661) #lmc_port=conf['lmc_port'])
    # Connect to spectrometer
    spectrometer.connect()

    bandwidth = 200e6
    nof_channels = 16384
    channel_width = bandwidth / 16384
    frequency_lo = int(options.channel_id) * channel_width - float(options.span) * channel_width
    step = 2 * float(options.span) * channel_width / int(options.nof_point)

    spectrometer._tile.test_generator_input_select(0xFFFFFFFF)

    print(channel_width)
    print(frequency_lo)
    print(options.span)
    print(step)

    channel_id = int(options.channel_id)
    channel_shape = []
    for n in range(int(options.nof_point)):
        freq = frequency_lo + n * step
        print("Step: " + str(n))
        print("Frequency: " + str(freq))
        spectrometer._tile.test_generator_set_tone(0, freq, 0.5)
        time.sleep(1.5)

        sock = rx_socket.open()
        spectrum = spead_rx_inst.run(sock, 1, 1)
        rx_socket.close()

        power_sx = 10 * np.log10(spectrum[0, channel_id - 1])
        power = 10 * np.log10(spectrum[0, channel_id])
        power_dx = 10 * np.log10(spectrum[0, channel_id + 1])

        print(power_sx)
        print(power)
        print(power_dx)

        channel_shape.append(power)

    plt.plot(channel_shape)
    plt.show()
    time.sleep(1000000)

