from matplotlib import pyplot as plt
import numpy as np
import logging
import os

from pyreach.digital_backend.spectrometer import Spectrometer
from pyreach.instrument.ucontroller.microcontroller import Microcontroller
from pyreach.reach_config import REACHConfig


class AcquireQTerm:

    def __init__(self, nof_integrations):
        """ Class initializer """
        self._ucontroller = None
        self._spectrometer = None
        self._integration_time = nof_integrations

        self._initialise_ucontroller()
        self._initialise_spectrometer()

    def _initialise_ucontroller(self):
        """ Initialise ucontroller """

        if self._ucontroller is None:
            self._ucontroller = Microcontroller()
            logging.info("Initialised ucontroller")

    def close(self):
        if self._ucontroller is not None:
            self._ucontroller.turn_off()

    def _initialise_spectrometer(self):
        """ Initialise spectrometer """

        if self._spectrometer is not None:
            return

        # Create spectrometer instance
        conf = REACHConfig()['spectrometer']
        self._spectrometer = Spectrometer(ip=conf['ip'], port=conf['port'],
                                          lmc_ip=conf['lmc_ip'], lmc_port=conf['lmc_port'])
        self._spectrometer.connect()

        if not self._spectrometer.is_programmed():

            if not self._spectrometer.is_programmed():
                logging.info("Spectrometer is not programmed, initialising")
            else:
                logging.info("Initialising spectrometer")

            bitstream = os.path.join(os.environ['REACH_CONFIG_DIRECTORY'], conf['bitstream'])
            self._spectrometer.program(bitstream)
            self._spectrometer.initialise(channel_truncation=conf['channel_truncation'],
                                          integration_time=conf['integration_time'],
                                          ada_gain=conf['ada_gain'])

        logging.info("Initialised spectrometer")

    def acquire_spectra(self):
        """ Acquire the spectra for the specified source """

        # Get required spectra
        _, spectra = self._spectrometer.acquire_spectrum(channel=0,
                                                         nof_seconds=self._integration_time,
                                                         wait_seconds=1,
                                                         integrate=False)
        return spectra

    def acquire_data(self, source):
        """ Acquire data """

        logging.info(f"Using an integration time of {self._integration_time}")

        # Acquire load
        self._ucontroller.switch_to_source("50_ohm_cold")
        logging.info("Acquiring spectra for 50_ohm_cold")
        load_spectra = self.acquire_spectra()

        # Acquire noise
        self._ucontroller.switch_to_source("noise_source")
        logging.info("Acquiring spectra for noise_source")
        noise_spectra = self.acquire_spectra()

        # Acquire source
        self._ucontroller.switch_to_source(source)
        logging.info(f"Acquiring spectra for {source}")
        source_spectra = self.acquire_spectra()

        return load_spectra, noise_spectra, source_spectra


if __name__ == "__main__":
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("--config", dest="config_file", default="reach",
                      help="Configuration file (default: reach)")
    parser.add_option("-s", "--source", dest="source", default="antenna", type=str,
                      help="Source to measure (default: antenna)")
    parser.add_option("-t", dest="accumulate", default=1, type=int,
                      help="Integration time in seconds (default: 1)")
    (options, args) = parser.parse_args()

    # Sanity check on provided config file
    if not options.config_file.endswith(".yaml"):
        options.config_file += ".yaml"

    # Check that REACH_CONFIG_DIRECTORY is defined in the environment
    if "REACH_CONFIG_DIRECTORY" not in os.environ:
        print("REACH_CONFIG_DIRECTORY must be defined in the environment")
        exit()

    # Check if file exists
    full_path = os.path.join(os.environ['REACH_CONFIG_DIRECTORY'], options.config_file)
    if not os.path.exists(full_path) or not os.path.isfile(full_path):
        print(f"Provided file ({options.config_file}, fullpath {full_path}) could not be found or is not a file")
        exit()

    # Load configuration
    c = REACHConfig(options.config_file)

    # Perform acquisitions
    q_term_spectra = AcquireQTerm(options.accumulate)
    load_s, noise_s, source_s = q_term_spectra.acquire_data(options.source)

    # Compute Q
    q = (source_s - load_s) / (noise_s - load_s)

    # Generate frequency range
    frequencies = np.linspace(0, 200, 16384)

    # Plot
    plt.figure(figsize=(12, 8))
    plt.title("Normalized Spectra")
    plt.plot(frequencies, q[0], label=options.source)
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("Q")
    plt.legend()
    plt.show()

    # Turn off
    # q_term_spectra.close()
