import datetime
import time
from datetime import timezone
from optparse import OptionParser

from pyreach.digital_backend.tile_reach import Tile


class TPMTemperatureLogger:
    def __init__(self):
        _time = time.strftime("%Y%m%d_%H%M%S")
        self.txt_file_name = 'tpm_temperature_' + _time + '.txt'

    def run(self, tpm_ip="10.0.10.3", period=1):
        tile = Tile(ip=tpm_ip)
        tile.connect()

        while True:
            board_temperature = tile.get_temperature()
            print("Board temperature: " + str(board_temperature))

            dt = datetime.datetime.now(timezone.utc)

            utc_time = dt.replace(tzinfo=timezone.utc)
            utc_timestamp = utc_time.timestamp()

            file_object = open(self.txt_file_name, 'a+')
            file_object.write(str(utc_timestamp) + ", " + str(board_temperature) + "\n")
            file_object.close()

            time.sleep(period)


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-p",
                      dest="period",
                      default="1",
                      help="Sampling period in seconds")
    parser.add_option("--ip",
                      dest="tpm_ip",
                      default="10.0.10.3",
                      help="TPM IP for temperature reading")

    (options, args) = parser.parse_args()

    tpm_temperature_logger = TPMTemperatureLogger()
    tpm_temperature_logger.run(options.tpm_ip, int(options.period))
