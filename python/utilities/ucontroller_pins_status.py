#!/usr/bin/env python

from tabulate import tabulate

from pyreach.instrument.ucontroller.microcontroller import Microcontroller
from pyreach.reach_config import REACHConfig

conf = REACHConfig()['ucontroller']
ucontroller = Microcontroller(initialise=False)

name = ["", "TEC_EN", "SCL2",
        "SDA2", "6V_EN", "NS_PWR",
        "", "", "MS4-1",
        "MS4-2", "MS4-3", "MS4-4",
        "", "MS3-1", "MS3-2",
        "MS3-3", "MS3-4", "",
        "", "INT_FAN", "5V_EN",
        "12V", "24V", "MS1-1",
        "MS1-2", "MS1-3", "MS1-4",
        "MS1-5", "MS1-6", "MS1-7",
        "MS1-8", "", "MS2-1",
        "MS2-2", "MS2-3", "MS2-4",
        "MS2-5", "MS2-6"]

source = ["", "", "",
          "", "", "",
          "", "", "Open",
          "Short", "10 Ohm", "250 Ohm",
          "", "36 Ohm", "27 Ohm",
          "69 Ohm", "91 Ohm", "",
          "", "", "",
          "", "", "Antenna",
          "Thermal", "Noise", "50 Ohm",
          "25 Ohm", "100 Ohm", "MS3 Enable",
          "MS4 Enable", "MTS", "MTS Passsthrough",
          "LNA", "VNA Short", "VNA Open",
          "VNA Load", "VNA Test"]


def get_pin_status():
    pins = list(range(1, 54))
    status = ["High" if x == 1 else "Low" for x in ucontroller._get_multiple_gpios(pins)]
    values = zip(pins, name, source, status)

    return tabulate(values, headers=["GPIO", "Name", "Source", "Status"])


if __name__ == "__main__":
    print(get_pin_status())
