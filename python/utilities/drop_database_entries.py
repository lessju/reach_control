from pyreach.repository.models import MonitoringProbe, Temperature, Power, TPMStatus
from pyreach.instrument.devices import Device
from pyreach.reach_config import REACHConfig

import logging

if __name__ == "__main__":
    # Load REACH configuration
    REACHConfig()

    # Drop existing collections
    TPMStatus.drop_collection()
    Power.drop_collection()
    Temperature.drop_collection()

