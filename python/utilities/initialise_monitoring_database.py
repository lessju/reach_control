from pyreach.repository.models import MonitoringProbe
from pyreach.instrument.devices import Device
from pyreach.reach_config import REACHConfig

if __name__ == "__main__":
    # Load REACH configuration
    REACHConfig()

    # Drop existing collections
    MonitoringProbe.drop_collection()

    # ----------- Populate entries for TPM digital box ---------

    # Temperature sensor
    MonitoringProbe(device=Device.ADAPTIVE_JUNIOR,
                    location="Digital Box",
                    description="TPM temperature probe").save()

    # Power meter
    MonitoringProbe(device=Device.POWER_SENSOR,
                    location="Digital Box",
                    description="Measure power going into TPM channel 1").save()

    # ----------- Populate entries for Receiver ---------

    # uController temperature sensor
    for i, probe in enumerate(REACHConfig()['ucontroller']['temperature']):
        MonitoringProbe(device=Device.UCONTROLLER,
                        location=f"Receiver box, {probe}",
                        sensor_index=i,
                        description="Temperature sensor attached to ucontroller").save()

    # Cooling System
    MonitoringProbe(device=Device.TEC_TCM,
                    location="Cooling system").save()

    # Receiver sources
    for i, probe in enumerate(REACHConfig()['tc08_temperature_sensor']['channel_names']):
        MonitoringProbe(device=Device.TC08,
                        location=f"Receiver box, {probe}",
                        sensor_index=i).save()
