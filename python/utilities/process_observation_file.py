from matplotlib import pyplot as plt
from scipy import signal
import numpy as np
import h5py

# Set matplotlib settings
import matplotlib.style as style
style.use('tableau-colorblind10')   # Color-blind friendly
plt.set_cmap('viridis')             # Color-blind friendly

plt.rcParams['legend.handlelength'] = 5.0
plt.rcParams['xtick.direction'] = 'out'
plt.rcParams['ytick.direction'] = 'out'
plt.rcParams['axes.linewidth'] = 1.5
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 14

path = "/home/lessju/Desktop/2023_02_15_14_22/test_observation.hdf5"

sources = [('c12r27', r'27$\Omega$'),
           ('c12r36', r'36$\Omega$'),
           ('c12r69', r'69$\Omega$'),
           ('c12r91', r'91$\Omega$'),
           ('c25open', 'Open'),
           ('c25r10', r'10$\Omega$'),
           ('c25r250', r'27$\Omega$'),
           ('c25short', r'Short'),
           ('r100', r'100$\Omega$'),
           ('r25', r'25$\Omega$'),
           ('ant', r'Antenna')]

x = np.arange(16384) * 200 / 16384

with h5py.File(path, 'r') as f:
    for s, l in sources:
        d = f['observation_data'][f'{s}_spectra']
        plt.plot(x[3500:13900], d[0, 3500:13900] / 3.55331e17, label=l)
    plt.legend(ncol=2, loc='lower center')
    plt.ylabel("Normalised Power")
    plt.xlabel("Frequency (MHz)")
    plt.show()

labels = ["Junction", "MS1", "HOT", "MS3", "MS4", "Long cable",
          "Short cable", "LNA", "Antenna"]

with h5py.File(path, 'r') as f:

    timestamps = f['observation_metadata']['temperature_timestamps']

    for i, label in enumerate(labels):
        if i in [0, 4]:
            continue

        d = f['observation_metadata']['temperatures'][5:, i]
        plt.plot((timestamps[5:] - timestamps[5]) / 3600, d, label=label)
    plt.legend(ncol=2, loc="lower right")
    plt.xlabel("Hours since start")
    plt.ylabel("Delta temperature (K)")
    plt.grid()
    plt.show()