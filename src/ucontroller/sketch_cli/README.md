**Cloned (25 May 2021) from Tian Huang's work at Cambridge**  
https://github.com/ianmalcolm/sketch_cli

Modified for the Teensy 3.5 (ARM Cortex-M4) development board but should be easily ported to other arduino platforms.

Things to control:

* Two transfer switch
* One 8-way RF switch
* Two 6-way RF switch
* One 2-way RF switch
* One peltier device
* One temperature sensor
...

Based on work from Mads Aasvik, original link available from here:  
https://www.norwegiancreations.com/2018/02/creating-a-command-line-interface-in-arduinos-serial-monitor/
